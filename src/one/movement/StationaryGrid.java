/*
 * Copyright 2010 Aalto University, ComNet Released under GPLv3. See LICENSE.txt for details.
 */
package one.movement;

import java.util.Arrays;

import one.core.Coord;
import one.core.Settings;
import one.movement.MovementModel;
import one.movement.Path;

/**
 * A stationary movement model that arranges nodes in a grid.
 * @author Graeme Stevenson
 */
public class StationaryGrid extends MovementModel {
   /** Per node group setting for setting the location ({@value} ) */
   public static final int ORIGIN_X = 1550;

   public static final int ORIGIN_Y = 1000;

   public static final int X_INCREMENT = 300;

   public static final int Y_INCREMENT = 300;

   public static final int MAX_NODES_IN_LINE = 6;

   public static int[] CURRENT_COORDS = {ORIGIN_X, ORIGIN_Y};

   public static int NODES_IN_LINE = 0;

   public static boolean FIRST = true;

   private Coord loc;

   /** The location of the nodes */

   /**
    * Creates a new movement model based on a Settings object's settings.
    * @param s The Settings object where the settings are read from
    */
   public StationaryGrid(final Settings s) {
      super(s);
      this.loc = new Coord(CURRENT_COORDS[0], CURRENT_COORDS[1]);
   }

   /**
    * Copy constructor.
    * @param sm The StationaryMovement prototype
    */
   public StationaryGrid(final StationaryGrid sm) {
      super(sm);
      // Prototype - ignore
      if (FIRST) {
         this.loc = new Coord(CURRENT_COORDS[0], CURRENT_COORDS[1]);
         FIRST = false;
         NODES_IN_LINE++;
         return;
      }

      if (NODES_IN_LINE < MAX_NODES_IN_LINE) {
         CURRENT_COORDS[0] += X_INCREMENT;
         NODES_IN_LINE++;
      } else {
         CURRENT_COORDS[0] = ORIGIN_X;
         CURRENT_COORDS[1] += Y_INCREMENT;
         NODES_IN_LINE = 1;
      }
      this.loc = new Coord(CURRENT_COORDS[0], CURRENT_COORDS[1]);

   }

   /**
    * Returns the only location of this movement model
    * @return the only location of this movement model
    */
   @Override
   public Coord getInitialLocation() {
      return loc;
   }

   /**
    * Returns a single coordinate path (using the only possible coordinate)
    * @return a single coordinate path
    */
   @Override
   public Path getPath() {
      final Path p = new Path(0);
      p.addWaypoint(loc);
      return p;
   }

   @Override
   public double nextPathAvailable() {
      return Double.MAX_VALUE; // no new paths available
   }

   @Override
   public StationaryGrid replicate() {
      return new StationaryGrid(this);
   }

}
