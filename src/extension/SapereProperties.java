package extension;

import java.util.Random;

public class SapereProperties {
	
	/**
	 * An enum representing the different spreading modes
	 */
	public static enum Spreading {
		REGULAR, PROBABALISTIC, ADAPTIVE_PROBABALISITC, LOCATION_BASED, MIDDLEWARE_STANDARD, SWITCHING
	};
	
	//Counters for metrics.
	public static int MESSAGE_COUNTER = 0;
	
	//If true we assume real broadcast communication. 
	//The metrics coming from The ONE will be wrong. 
	public static boolean BROADCAST = true;
	
	//Chances of a node haveing GPS enabled.
	public static final double GPS_RATIO = 0.5f;
	
	// No longer required. 
	// Use simulator properties to configure eco law firing frequency
	public static int CYCLE_COUNTER = 0;
	
	public static float ECOLAW_FREQ = 30; //cycles number
	
	public static boolean BATCH_MODE = false;
	
	public static long SEED = 2;
	
	public static Random RANDOM = new Random(SEED);
	
	public static int RUN_NUMBER = 1;
	
	public static int location = 0;
	public static int prob = 0;
	
	public static int NODES_NUMBER = 140;
	
	public static final boolean QUIT_IF_SEGMENTED = false;
	
	public static boolean GRADIENTS_EVALUATION = true;
	
	/**
	 * The type of spreading to enable
	 */
	public static final Spreading SPREADING_MODE = Spreading.REGULAR;

	/**
	 * Whether to repeatedly propagate seen messages.
	 */
	public static final boolean PROPAGATE_ONCE_ONLY = true;

	/**
	 * The minimum probability threshold to be used.
	 */
	public static final float MINIMUM_PROBABILITY = 0.8f;
	
	/**
	 * Alpha changes the slope of the function that provides the adaptive Prob.
	 */
	public static int ALPHA = 8;
	/**
	 * The spreading probability
	 */
	public static float SPREADING_PROBABILITY = 0.88f;
	
	
	public static int LOCATION_THRESHOLD = 40;
	
	public static String DECAY_VALUE = "15";

	// Smaller map?
	public static final boolean SMALLER_MAP = false;

	// Limit of the smaller maps
//	public static double MAX_X_BOUND = 2552500;
//	public static double MAX_Y_BOUND = 6672600;
//
//	public static double MIN_X_BOUND = 2552400;
//	public static double MIN_Y_BOUND = 6672500;
	
	public static double MAX_X_BOUND = 4552500;
	public static double MAX_Y_BOUND = 8672600;

	public static double MIN_X_BOUND = 4552400;
	public static double MIN_Y_BOUND = 8672500;

	// SOME MATHS FOR REDUCING THE MAP

	// MAX AND MIN GPS POSITIONS
	// MAX_X = 2554569
	// MIN_X = 2550186

	// MAX_Y = 6674388
	// MIN_Y = 6671039

	// SIZE OF THE MAP IN GPS units
	// MAX_X - MIN_X = 4383
	// MAX_Y - MIN_Y = 3349

	// I am going to remove 1000 from each side.

	// public static double MAX_X_BOUND = 2553569;
	// public static double MAX_Y_BOUND = 6673388;

	// public static double MIN_X_BOUND = 2551186;
	// public static double MIN_Y_BOUND = 6672039;
	
	
	//Metrics for Chemotaxis.
	public static int hops = 0; // for counting the number of hops with Chemotaxis
	public static int messagesCreated = 0; //messeges that are created to follow the gradient
	public static int messageDelivered = 0; //Messages that are delivered to node 0. 
	

}
