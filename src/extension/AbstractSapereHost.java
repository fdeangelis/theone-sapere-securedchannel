// [Project Title]
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package extension;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.core.Application;
import one.core.Connection;
import one.core.DTNHost;
import one.core.Message;
import one.core.Settings;
import sapere.lsa.Id;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.node.NodeManager;
import sapere.node.lsaspace.Operation;

/**
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public abstract class AbstractSapereHost extends Application {

	/**
	 * The application ID for a sapere host in The One.
	 */
	public static final String APP_ID = "SapereHost";

	/**
	 * The node manager.
	 */
	private NodeManager my_nodeManager;

	boolean my_executionSwitch = true;

	/**
	 * The map containing the neighbouring nodes and their LSAs
	 */
	private Map<String, Lsa> my_neighbourLSAs = new HashMap<String, Lsa>();

	/**
	 * The node hosting the application.
	 */
	private DTNHost my_host;

	/**
	 * Whether we have setup the application or not.
	 */
	private boolean my_initialised;

	/**
	 * The application settings.
	 */
	private final Settings my_settings;

	/**
	 * Creates a new SAPERE host.
	 * 
	 * @param some_settings
	 *            the settings file containing application settings.
	 */
	public AbstractSapereHost(final Settings some_settings) {
		super();
		my_settings = some_settings;
		super.setAppID(APP_ID);
	}

	/**
	 * Extracts the LSA from any received message and passes it to the
	 * NetworkReceiverManager for injection into the local LSA Space.
	 * 
	 * @param a_message
	 *            the received message.
	 * @param the_localHost
	 *            the ? host.
	 * @return this method always returns null to indicate that the message
	 *         should be dropped. As direct routing is the only type allowed,
	 *         this should always result in the correct behaviour (i.e.,
	 *         matching the middleware).
	 */
	@Override
	public Message handle(final Message a_message, DTNHost the_localHost) {
		// System.err.println(" From:" + a_message.getFrom().getName() + " To:"
		// +
		// a_message.getTo().getName() + " "
		// + a_message.getProperty("lsa"));
		Lsa receivedLsa = (Lsa) a_message.getProperty("lsa");
		if (receivedLsa != null) {
			my_nodeManager.getNetworkReceiverManager().doInject(receivedLsa);
		}
		return null;
	}

	/**
	 * A delegate method that calls the operation manager, eco-law engine on
	 * alternate cycles, and calls implementing classes on each cycle.
	 */
	public void sapereUpdate(final DTNHost host) {

		// Update the neighbouring space LSAs
		updateNeighbours(host);

		// Call subclasses
		onSimulationCycle();

		// Run the middleware engines
		my_nodeManager.getOperationManager().run();
		my_nodeManager.getEcoLawsEngine().run();
	}

	/**
	 * A delegate method that calls the operation manager, eco-law engine on
	 * alternate cycles, and calls implementing classes on each cycle.
	 */
	@Override
	public void update(final DTNHost host) {
		// Performs the initial initialisation. Otherwise we use the
		// sapereUpdate() method.
		if (!my_initialised) {
			initialiseHost(host);
			my_initialised = true;
		}
		
//		if (SapereProperties.CYCLE_COUNTER % 200 == 0){
//			Class<? extends AbstractSapereHost> hostClass = getClass();
//			String hostName = getHost().getName();
//			try {
//				final Method setupMethod = hostClass.getMethod("setup" + hostName);
//				setupMethod.invoke(this);
//				//System.out.println(hostName + " setup complete");
//			} catch (Exception e) {
//				// The setup method doesn't exist, so we skip over it.
//				//System.out.println("No individual setup specified for "+ hostName);
//			}
//		}
	}

	/**
	 * @param host
	 */
	private void initialiseHost(DTNHost a_host) {
		my_host = a_host;
		// Create the node manager
		my_nodeManager = new NodeManager(a_host, my_settings);
		onSetup();
	}

	/**
	 * Perform setup operations, like injecting an LSA or stating an agent. This
	 * method used reflection trickery to find methods in implementing classes
	 * corresponding to the node name.
	 */
	public final void onSetup() {
		// 1. Get a reference to the class and host name
		Class<? extends AbstractSapereHost> hostClass = getClass();
		String hostName = getHost().getName();

		// 2. Invoke the common setup method for all nodes if it exists.
		try {
			final Method setupMethod = hostClass.getMethod("setupAll");
			setupMethod.invoke(this);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 3. Look for a method corresponding to the individual host name and
		// invoke it.
		try {
			final Method setupMethod = hostClass.getMethod("setup" + hostName);
			setupMethod.invoke(this);
			//System.out.println(hostName + " setup complete");
		} catch (Exception e) {
			// The setup method doesn't exist, so we skip over it.
			//System.out.println("No individual setup specified for "+ hostName);
		}
	}

	/**
	 * Perform operations during the simulation cycle
	 */
	public abstract void onSimulationCycle();

	/**
	 * Makes the GUI visible.
	 */
	public void setConsoleVisible() {
		if (!(my_nodeManager == null)) {
			this.my_nodeManager.setConsoleVisible();
		}
	}

	/**
	 * Gets the NodeManager for this Sapere host.
	 * 
	 * @return the node manager.
	 */
	public NodeManager getNodeManager() {
		return my_nodeManager;
	}

	/**
	 * Updates all the neighbour LSAs in the host based on the simulator state.
	 * 
	 * @param host
	 */
	private void updateNeighbours(final DTNHost host) {
		// Create the list of nodes this node is connected to.
		final ArrayList<String> neighbouringNodeNameList = new ArrayList<String>();
		final List<Connection> connectionList = host.getConnections();
		for (final Connection connection : connectionList) {
			neighbouringNodeNameList.add(connection.getOtherNode(host)
					.getName());
		}

		// Create LSAs for each *new* neighbour
		for (final String neighbourName : neighbouringNodeNameList) {
			if (!my_neighbourLSAs.containsKey(neighbourName)) {
				final Lsa lsa = new Lsa();
				// lsa.addProperty(new Property("name", neighbourName));
				lsa.addProperty(new Property("neighbour", neighbourName));
				final Operation operation = new Operation().injectOperation(
						lsa, "neighbour_lsa_agent", null);
				Id id = my_nodeManager.getOperationManager().execOperation(
						operation);
				lsa.setId(id);
				my_neighbourLSAs.put(neighbourName, lsa);
			}
		}

		// Remove LSAs for neighbours no longer connected.
		final List<String> departedNeigbours = new ArrayList<String>();

		for (Map.Entry<String, Lsa> entry : my_neighbourLSAs.entrySet()) {
			if (!neighbouringNodeNameList.contains(entry.getKey())) {
				final Operation operation = new Operation().removeOperation(
						entry.getValue().getId(), "neighbour_lsa_agent");
				my_nodeManager.getOperationManager().execOperation(operation);
				departedNeigbours.add(entry.getKey());
			}
		}

		for (String removeName : departedNeigbours) {
			my_neighbourLSAs.remove(removeName);
		}
	}

	/**
	 * Return the node hosting the application.
	 * 
	 * @return the host node.
	 */
	public DTNHost getHost() {
		return my_host;
	}

	/**
	 * Reruns the application settings file.
	 * 
	 * @return the settings.
	 */
	public Settings getSettings() {
		return my_settings;
	}
	
	

}
