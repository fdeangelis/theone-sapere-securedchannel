/*
 * Chemotaxis Agent.
 * 
 * GPL v.3
 * 
 * Francesco De Angelis
 * francesco.deangelis@unige.ch
 */
package saperedemo;

import java.util.UUID;
import java.util.Vector;
import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.lsa.SubDescription;
import sapere.lsa.interfaces.ILsa;
import sapere.node.lsaspace.OperationManager;
import sapere.node.notifier.Notifier;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

/* Chemotaxis agent.
 * To use in association with a Dynamic Gradient Agent: DGAgent
 */
public class CAgent extends SapereAgent {

	// the gradient id which this chemotaxis is associated with
	private String gradientId;
	private Vector<Property> props;
	public CAgent(String name, OperationManager an_opMng, Notifier notifier,
				 String gradientId,
				 Vector<Property> props) {
		super("CAgent" + name, an_opMng, notifier);
		// the gradient id which this chemo. is created for
		this.gradientId = gradientId;
		// copy all the properties
		this.props = (Vector<Property>)props.clone();
	}
	
	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) {
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onReadNotification(ReadEvent readEvent) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setInitialLSA() {
		createLsa();
	}
	
	public void createLsa()
	{
		for (Property p : this.props)
			lsa.addProperty(p);
		// add chemotaxis properties
		addChemotaxis();
		lsa.addProperty(new Property("CGradient", this.agentName));
		submitOperation();
	}
	
	private void addChemotaxis()
	{
		lsa.addProperty(new Property("name", this.agentName));
		lsa.addProperty(new Property("chemotaxis", this.gradientId));
		lsa.addProperty(new Property("uuid", UUID.randomUUID().toString()));
	}
	
	protected boolean isExternalEvent(ILsa iLsa)
	{
		//if (1 == 1)return false;
		if (iLsa == null) return false;
		SubDescription s = iLsa.getSubDescriptionByName("q");
		if (s == null) return true;
		Property p = s.getProperty("hopsTravelled");
		if (p == null) return true;
		return false;
	}
}
