package saperedemo;

import one.core.DTNHost;
import sapere.agent.SapereAgent;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class AgentEmpty extends SapereAgent {

   private final String my_nodeName;

   private final String my_value;
   
   private DTNHost host;

   /**
    * Creates a new agent.
    * @param name the name of the node
    * @param an_opMng the operation manager for the node.
    */
   public AgentEmpty(String name, NodeManager a_nodeManager, String a_value, DTNHost host) {
      super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
      my_nodeName = name;
      my_value = a_value;
      this.host = host;
   }

   /**
    * {@inheritDoc}
    */
   public void onBondAddedNotification(BondAddedEvent event)
   {
     
   }

   /**
    * {@inheritDoc}
    */
   public void onBondRemovedNotification(BondRemovedEvent event) 
   {
	   //this.host.hasChemo = false;
   }

   /**
    * {@inheritDoc}
    */
   public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
      // TODO Auto-generated method stub
   }

   /**
    * {@inheritDoc}
    */
   public void onPropagationEvent(PropagationEvent event) {
   }

   /**
    * {@inheritDoc}
    */
   public void setInitialLSA() {
   }

   public void onReadNotification(ReadEvent readEvent) {
	// TODO Auto-generated method stub
	
   }

   public void onDecayedNotification(DecayedEvent event) {
	// TODO Auto-generated method stub
	
   }
}