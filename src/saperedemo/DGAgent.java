/* 
 * Dynamic Gradient agent
 * 
 * GPL v.3
 *  
 * Francesco De Angelis
 * francesco.deangelis@unige.ch
 */
package saperedemo;

import java.util.UUID;
import extension.SapereProperties;
import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.lsa.SubDescription;
import sapere.lsa.interfaces.ILsa;
import sapere.node.lsaspace.OperationManager;
import sapere.node.notifier.Notifier;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class DGAgent extends SapereAgent {

	protected enum LSAInit
	{	START,  			// starting the agent
		DECAY_RESTART	//  re-injecting lsa after a decay event
	};			  
	private boolean keepSpreading;
	
	public DGAgent(String name, OperationManager an_opMng, Notifier notifier) {
		super(name, an_opMng, notifier);
		keepSpreading = true;
	}
	
	public void addDynamicGradient(String gradientId)
	{
		// add the properties for the dynamic gradient 
		//
		lsa.addProperty(new Property("agent-name", this.agentName));
		lsa.addProperty(new Property("name", gradientId));
		// each gradient must have an unique index
		lsa.addProperty(new Property("gradient", gradientId));
		lsa.addProperty(new Property("uuid", UUID.randomUUID().toString()));
		lsa.addProperty(new Property("spread", "true"));
		lsa.addProperty(new Property("previous", "local"));
		lsa.addProperty(new Property("hopsTravelled", "0"));
		lsa.addSubDescription("q", new Property("data-value", "datum"));
		lsa.addProperty(new Property("decay", SapereProperties.DECAY_VALUE));
	}
	
	public static boolean isItGradient(ILsa lsa)
	{
		if (lsa.getProperty("name")!= null
			&& lsa.getProperty("uuid") != null
			&& lsa.getProperty("spread") != null
			&& lsa.getProperty("spread").getValue().get(0).equals("true")
			&& lsa.getProperty("previous") != null
			&& lsa.getProperty("hopsTravelled") != null)
			return true;
		else
			return false;
	}
	
	public static boolean isExternalEvent(ILsa iLsa)
	{
		//if (1 == 1)return false;
		if (iLsa == null) return false;
		SubDescription s = iLsa.getSubDescriptionByName("q");
		if (s == null) return true;
		Property p = s.getProperty("hopsTravelled");
		if (p == null) return true;
		return false;
	}
	
	/*
	 * Get the gradient index (used also with the chemotaxis agent)
	 */
	public static String getGradientId(ILsa lsa)
	{
		if (lsa.getProperty("name")!= null)
			return lsa.getProperty("name").getValue().get(0);
		else
			return "";
	}
	
	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
	   this.lsaId = null;
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) {
		this.lsaId = null;
		if (keepSpreading == true)
			createLsa(LSAInit.DECAY_RESTART);
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onReadNotification(ReadEvent readEvent) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setInitialLSA() {
		// TODO Auto-generated method stub
		createLsa(LSAInit.START);
	}
	
	public void createLsa(LSAInit reason)
	{
		if (reason == LSAInit.START)
		{
			// this gradient is created for the first time
		}
		else if (reason == LSAInit.DECAY_RESTART)
		{
			// this gradient is re-injected
			// after after a decay
		}
	}
	
	public void destroy()
	{
		// the gradient will not be reinjected 
		// after the next decay
		keepSpreading = false;
	}
}
