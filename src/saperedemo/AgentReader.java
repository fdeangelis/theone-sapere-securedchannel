/*
 * Receives a gradient with a public key and generates
 * a new encrypted message by using it.
 * 
 * GPL v.3
 * 
 * Francesco De Angelis
 * 
 * francesco.deangelis@unige.ch
 * 
 * 
 */

package saperedemo;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Vector;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;

import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.lsa.SubDescription;
import sapere.lsa.interfaces.ILsa;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;
import saperedemo.securitymanager.EncryptedMessage;
import saperedemo.securitymanager.KeyManager;

public class AgentReader extends SapereAgent {

   private static final Logger LOGGER = Logger.getLogger("extension.agent.example");
   private NodeManager nManager;
   private String nodeName;

   /**
    * Creates a new agent.
    * @param name the name of the node
    * @param an_opMng the operation manager for the node.
    */
   public AgentReader(String agentName, String nodeName, NodeManager a_nodeManager) {
	  super(agentName, 
			  a_nodeManager.getOperationManager(), 
			  a_nodeManager.getNotifier());
      this.nodeName = nodeName;
      this.nManager = a_nodeManager;
   }
   
   public void onBondRemovedNotification(BondRemovedEvent event) {
	   //System.out.println(agentName + " bondRemoved");
   }
   public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
	   if (isLsaInteresting(event.getLsa()) == false) return;
	   //System.out.println("bond updated");
   }
   
   public void onPropagationEvent(PropagationEvent event) {
   }
   
   private void createChemotaxis(String gId, ILsa lsa)
   {
	   String sender = lsa.getProperty("agent-name").getValue().get(0);
	   // get the public key of the sender
	   String publicKey = lsa.getProperty(Const.PROP_PUB_KEY).getValue().get(0);
	   
	   // generate the response (encrypted mex)
	   String response = generateEncMex(publicKey,sender);
	   
	   Vector<Property> pv = new Vector<Property>();
	   Property p = new Property(Const.PROP_ENC_MEX,response);
	   pv.add(p);
	   // create a chemotaxis agent
	   CAgent chemo = new CAgent(agentName + "chemo", 
			   nManager.getOperationManager(), 
			   nManager.getNotifier(), 
			   gId, pv);
	   chemo.setInitialLSA();
	   System.out.println("Node25 created encrypted message");
   }
   
   public String generateEncMex(String pk, String sender)
   {
	   String response;
	   String toEncrypt = "I'm Node25, this is a secret message for Node0";
	   try
	   {
		   // decode the public key
		   KeySpec kSpec = new X509EncodedKeySpec(Base64.decodeBase64(pk));
		   
		   // re-generate the public key
		   PublicKey decPk = KeyFactory.getInstance(KeyManager.PB_ALG).generatePublic(kSpec);
		   KeyManager key = new KeyManager();
		   // encrypt the message
		   EncryptedMessage enc = key.encMessage(toEncrypt, decPk);
		   // encode the whole message
		   response = enc.encoded();
	   }
	   catch(Exception ex)
	   {
		   response = ex.getMessage();
	   }
	   return response;
   }
   
   public void onReadNotification(ReadEvent readEvent) {
	   //System.out.println(agentName + " bondUpdate");
   }

   public void onDecayedNotification(DecayedEvent event) {	
	   this.lsaId = null;
   }

   @Override
	public void onBondAddedNotification(BondAddedEvent event) {
	   if (isExternalEvent(event.getBondedLsa()) == false) return; 
	   // we have found a gradient
	   if (DGAgent.isItGradient(event.getBondedLsa()))
	   {
		   // get the gradient id for the chemotaxis response
		   String gId = DGAgent.getGradientId(event.getBondedLsa());
		   createChemotaxis(gId, event.getBondedLsa());	   
	   }
}

	@Override
	public void setInitialLSA() {
		// TODO Auto-generated method stub
	   //addDirectPropagation("Node1");
	   lsa.addProperty(new Property("Agent-hh", this.agentName));
	   //lsa.addProperty(new Property("uuid", UUID.randomUUID().toString()));
	   lsa.addSubDescription("SubProp", new Property(Const.PROP_PUB_KEY,"*"));
       submitOperation();
	}
	
	protected boolean isExternalEvent(ILsa iLsa)
	{
		//if (1 == 1)return false;
		if (iLsa == null) return false;
		SubDescription s = iLsa.getSubDescriptionByName("q");
		if (s == null) return true;
		Property p = s.getProperty("hopsTravelled");
		if (p == null) return true;
		return false;
	}
	
   public boolean isLsaInteresting(ILsa lsa)
   {
	   if (DGAgent.isExternalEvent(lsa) == false) return false;
	   
	   if (lsa.getSubDescriptionByName("SubProp") != null &&
			   	lsa.getProperty(Const.PROP_PUB_KEY) 
			   != null)
		   return true;
	   
	   return false;
   }
	
}
