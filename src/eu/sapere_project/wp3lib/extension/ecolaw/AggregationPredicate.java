// [Project Title]
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package eu.sapere_project.wp3lib.extension.ecolaw;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import one.core.SimClock;
import sapere.lsa.Lsa;
import sapere.lsa.SubDescription;
import sapere.lsa.values.PropertyName;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

import eu.sapere_project.wp3lib.extension.ecolaw.filter.Filter;

/**
 * A predicate for examining if an LSA should have the aggregation eco-law applied to it, with
 * parameters to filter on source and aggregation type.
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public final class AggregationPredicate implements Predicate<Lsa> {

   /**
    * The possible aggregation operators.
    */
   public enum OP {
      MIN, MAX, AVG, DST
   };

   /**
    * An aggregation operator to filter on.
    */
   private final OP my_operator;

   /**
    * A filter to group a set of LSAs for aggregation.
    */
   private List<Filter> my_propertyFilters = Lists.newLinkedList();

   /**
    * Creates a new aggregation predicate with parameter for filtering on operation type.
    * @param an_aggregationOperator the aggregation operator.
    */
   private AggregationPredicate(final OP an_aggregationOperator) {
      my_operator = checkNotNull(an_aggregationOperator);
   }

   /**
    * Creates a new aggregation predicate with parameter for filtering on operation type.
    * @param an_aggregationOperator the aggregation operator.
    * @return the Aggregation Operator.
    */
   public static AggregationPredicate create(final OP an_aggregationOperator) {
      return new AggregationPredicate(an_aggregationOperator);
   }

   /**
    * Adds a filter to the aggregation predicate.
    * @param a_filter the filter to apply to this LSA.
    * @return this object for a fluid interface.
    */
   public AggregationPredicate with(final Filter a_filter) {
      my_propertyFilters.add(a_filter);
      return this;
   }

   /**
    * Checks an LSA for the necessary aggregation properties.
    * @param an_lsa the LSA to filter.
    * @return true if the LSA matches the filter, false otherwise.
    */
   @Override
   public boolean apply(final Lsa an_lsa) {

//      if (OP.DST.equals(my_operator) && SimClock.getIntTime() > 60 && an_lsa.getId().toString().startsWith("node7")
//            && my_propertyFilters.size()>0) {
//         System.err.println("");
//      }

      for (SubDescription sub : an_lsa.getSubDescriptions()) {
         // 1. Check the basic aggregation properties in sub description (and that the indicated
         // field value exists).
         if (!(sub.getSingleValue(PropertyName.AGGREGATION_OP.toString()).isPresent()
               && sub.getSingleValue(PropertyName.FIELD_VALUE.toString()).isPresent() && an_lsa.getSingleValue(
               sub.getSingleValue(PropertyName.FIELD_VALUE.toString()).get()).isPresent())) {
            continue;
         }

         // 2. Check the aggregation operator matches the subdescription.
         if (!my_operator.name().equals(sub.getSingleValue(PropertyName.AGGREGATION_OP.toString()).get())) {
            continue;

         }

         // 3. Check the property filter matches.
         boolean matches = true;
         for (Filter filter : my_propertyFilters) {
            matches = filter.apply(an_lsa);
            if (!matches) {
               break;
            }
         }
         if (!matches) {
            continue;
         }

         // 4. All checks pass.
         return true;

      }
      return false;
   }
}
