// [Project Title]
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package eu.sapere_project.wp3lib.extension.ecolaw.filter;

import static com.google.common.base.Optional.fromNullable;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import sapere.lsa.Lsa;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * A property filter used as part of an eco-law matcher.
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public class PropertyFilter implements Filter {

   /**
    * The allowable predicates that can be used to do a comparison.
    */
   public enum Predicate {
      EXISTS, DOES_NOT_EXIST, EQUALS, DOES_NOT_EQUAL, CONTAINS, DOES_NOT_CONTAIN,
   };

   /**
    * The property name to match against.
    */
   private final String my_propertyName;

   /**
    * The predicate to match the value against.
    */
   private final Predicate my_predicate;

   /**
    * The (optional) value of the property to match using the given predicate.
    */
   public final Optional<String> my_propertyValue;

   /**
    * Creates a new filter matching a names property value using a given predicate.
    * @param a_propertyName the property to match against
    * @param a_predicate the predicate to match the value.
    * @param a_propertyValue the property value to match using the predicate.
    */
   private PropertyFilter(final String a_propertyName, final Predicate a_predicate, final String a_propertyValue) {
      my_propertyName = checkNotNull(a_propertyName);
      my_predicate = checkNotNull(a_predicate);
      my_propertyValue = fromNullable(a_propertyValue);
   }

   /**
    * Creates a new filter matching on the existence of a property.
    * @param a_propertyName the property to check.
    * @return a filter matching the input criteria.
    */
   public static PropertyFilter exists(final String a_propertyName) {
      return new PropertyFilter(a_propertyName, Predicate.EXISTS, null);
   }

   /**
    * Creates a new filter matching on the none existence of a property.
    * @param a_propertyName the property to check.
    * @return a filter matching the input criteria.
    */
   public static Filter doesNotExist(final String a_propertyName) {
      return new PropertyFilter(a_propertyName, Predicate.DOES_NOT_EXIST, null);
   }

   /**
    * Creates a new filter matching on the equality of a property value.
    * @param a_propertyName the property to check.
    * @param a_value the value to check the filter against.
    * @return a filter matching the input criteria.
    */
   public static PropertyFilter equals(final String a_propertyName, final String a_value) {
      return new PropertyFilter(a_propertyName, Predicate.EQUALS, a_value);
   }

   /**
    * Creates a new filter matching on the inequality of a property value.
    * @param a_propertyName the property to check.
    * @param a_value the value to check the filter against.
    * @return a filter matching the input criteria.
    */
   public static Filter doesNotEqual(final String a_propertyName, final String a_value) {
      return new PropertyFilter(a_propertyName, Predicate.DOES_NOT_EQUAL, a_value);
   }

   /**
    * Creates a new filter matching on the lexical containment of a value.
    * @param a_propertyName the property to check.
    * @param a_value the value to check the filter against.
    * @return a filter matching the input criteria.
    */
   public static Filter contains(final String a_propertyName, final String a_value) {
      return new PropertyFilter(a_propertyName, Predicate.CONTAINS, a_value);
   }

   /**
    * Creates a new filter matching on the lexical non-containment of a property value.
    * @param a_propertyName the property to check.
    * @param a_value the value to check the filter against.
    * @return a filter matching the input criteria.
    */
   public static Filter doesNotContain(final String a_propertyName, final String a_value) {
      return new PropertyFilter(a_propertyName, Predicate.DOES_NOT_CONTAIN, a_value);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean apply(final Lsa an_lsa) {
      switch (my_predicate) {
         case EXISTS:
            return an_lsa.hasProperty(my_propertyName);
         case DOES_NOT_EXIST:
            return !an_lsa.hasProperty(my_propertyName);
         case EQUALS:
            return an_lsa.hasProperty(my_propertyName)
                  && an_lsa.getSingleValue(my_propertyName).get().equals(my_propertyValue.get());
         case DOES_NOT_EQUAL:
            return an_lsa.hasProperty(my_propertyName)
                  && !an_lsa.getSingleValue(my_propertyName).get().equals(my_propertyValue.get());
         case CONTAINS:
            return an_lsa.hasProperty(my_propertyName) && !doesNotContain(an_lsa);
         case DOES_NOT_CONTAIN:
            return an_lsa.hasProperty(my_propertyName) && doesNotContain(an_lsa);
         default:
            return false;
      }
   }

   /**
    * Checks to see if any of the components of a property value (as separated by :) are present in
    * the LSA.
    * @param an_lsa the LSA to be filtered
    * @return true if any components are present, false otherwise.
    */
   public boolean doesNotContain(final Lsa an_lsa) {
      // 1. Form Set from sources in template.
      final Set<String> templateSources = Sets.newHashSet(Arrays.asList(my_propertyValue.get().split(":")));

      // 2. Form Set from sources in LSA.
      final Set<String> lsaSources = Sets.newHashSet(Arrays.asList(an_lsa.getSingleValue(my_propertyName).get()
            .split(":")));

      // 3. Check if the sets of sources are disjoint.
      return Collections.disjoint(templateSources, lsaSources);

   }
}
