// Taken from the SAPERE WP3 Libraries
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package eu.sapere_project.wp3lib.extension.templates;

import sapere.lsa.Property;
import sapere.lsa.SubDescription;
import sapere.lsa.interfaces.ILsa;
import sapere.lsa.values.PropertyName;
import eu.sapere_project.wp3lib.extension.ecolaw.AggregationPredicate.OP;

/**
 * Applies aggregation to an LSA value.
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public class AggregationInvoker implements Pattern {

   /**
    * The common property.
    */
   private final String my_commonProperty;

   /**
    * The aggregation field.
    */
   private final String my_aggregationField;

   /**
    * The aggregation mode.
    */
   private final OP my_aggregationOperator;

   /**
    * The source name for the gradient.
    */
   private final String my_commonPropertyValue;

   /**
    * Creates an aggregation template using a field and mode.
    * @param an_aggregationField the field to apply the aggregation to.
    * @param an_aggregationOperator the aggregation operation to apply to the field.
    */
   public AggregationInvoker(final String a_commonProperty, final String a_commonPropertyValue,
         final String an_aggregationField, OP an_aggregationOperator) {
      my_commonProperty = a_commonProperty;
      my_commonPropertyValue = a_commonPropertyValue;
      my_aggregationField = an_aggregationField;
      my_aggregationOperator = an_aggregationOperator;
   }

   /**
    * {@inheritDoc}
    */
   public void applyToLSA(final ILsa an_lsa) {
      final SubDescription sub = an_lsa.addSubDescription("agg:" + my_aggregationOperator.name());
      sub.addProperty(new Property(my_commonProperty, my_commonPropertyValue));
      sub.addProperty(new Property(PropertyName.AGGREGATION_OP.toString(), my_aggregationOperator.name()));
      sub.addProperty(new Property(PropertyName.FIELD_VALUE.toString(), my_aggregationField));
   }

   /**
    * {@inheritDoc}
    */
   public void removeFromLSA(final ILsa an_lsa) {
      an_lsa.removeSubDescription("agg:" + my_aggregationOperator.name());
   }

}
