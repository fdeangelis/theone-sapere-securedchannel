// Taken from the SAPERE WP3 Libraries
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package eu.sapere_project.wp3lib.extension.templates;

import sapere.lsa.interfaces.ILsa;
import eu.sapere_project.wp3lib.extension.ecolaw.AggregationPredicate.OP;

/**
 * Applies chemotaxis to an LSA.
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public final class ResponseInvoker implements Pattern {

   /**
    * The source of the response.
    */
   private final String my_source;

   /**
    * The destination for the response.
    */
   private final String my_destination;

   /**
    * The response Id.
    */
   private final String my_responseId;

   /**
    * Creates an response invoker for a source.
    * @param a_source the source of the response.
    * @param a_destination the destination for the response.
    * @param a_responseId an identifier for the response corresponding to the match.
    */
   public ResponseInvoker(final String a_source, final String a_destination, final String a_responseId) {
      my_source = a_source;
      my_destination = a_destination;
      my_responseId = a_responseId;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void applyToLSA(final ILsa an_lsa) {
      // Add decay
      DecayInvoker.DECAY_50.applyToLSA(an_lsa);
      // Aggregate most recent from source
      new AggregationInvoker("source", my_source, "decay", OP.MAX).applyToLSA(an_lsa);
      // Aggregate using DST with others going to same destination
      new AggregationInvoker("chemotaxisResponseId", my_responseId, "complete_classification", OP.DST)
            .applyToLSA(an_lsa);
      // Apply chemotaxis to route to the destination.
      new ChemotaxisInvoker(my_source, my_destination, my_responseId).applyToLSA(an_lsa);
   }

   /**
    * Utility method to apply to an LSA and return the template to support assignment.
    * @param a_source the source of the response.
    * @param a_destination the destination for the response.
    * @param a_responseId an identifier for the response corresponding to the match.
    * @param an_lsa the LSA to apply the invoker to.
    * @return the invoker, after application to the LSA.
    */
   public static ResponseInvoker createAndApply(final String a_source, final String a_destination,
         final String a_responseId, final ILsa an_lsa) {
      final ResponseInvoker result = new ResponseInvoker(a_source, a_destination, a_responseId);
      result.applyToLSA(an_lsa);
      return result;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void removeFromLSA(final ILsa an_lsa) {
      DecayInvoker.DECAY_100.removeFromLSA(an_lsa);
      new AggregationInvoker("source", my_source, "decay", OP.MAX).removeFromLSA(an_lsa);
      new AggregationInvoker("chemotaxisDestination", my_destination, "complete_classification", OP.DST)
            .removeFromLSA(an_lsa);
      new ChemotaxisInvoker(my_source, my_destination, my_responseId).removeFromLSA(an_lsa);
   }

}
