package eu.sapere_project.wp3lib.application;

import java.util.LinkedList;
import java.util.List;

import one.core.Settings;
import sapere.agent.SapereAgent;
import sapere.node.lsaspace.ecolaws.Bonding;
import sapere.node.lsaspace.ecolaws.Chemotaxis3;
import sapere.node.lsaspace.ecolaws.Decay;
import sapere.node.lsaspace.ecolaws.IEcoLaw;
import sapere.node.lsaspace.ecolaws.Propagation;
import sapere.node.lsaspace.ecolaws.spreading.AdaptiveProbabilisticSpreading;
import sapere.node.lsaspace.ecolaws.spreading.LocationBasedSpreading;
import sapere.node.lsaspace.ecolaws.spreading.ProbabilisticSpreading;
import sapere.node.lsaspace.ecolaws.spreading.RegularSpreading;
import sapere.node.lsaspace.ecolaws.spreading.SwitchingSpreading;
import eu.sapere_project.wp2lib.AgentChemoChecker;
import eu.sapere_project.wp2lib.ChemotaxisManager;
import eu.sapere_project.wp3lib.extension.ecolaw.Aggregation2;
import extension.AbstractSapereHost;
import extension.SapereProperties;
import extension.SimulationUpdatable;


/**
 * A superclass that performs common functionality for WP3Applications.
 * 
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk).
 */
public abstract class SapereApplication extends AbstractSapereHost {

	
	/**
	 * The list of agents to update in the simulation cycle.
	 */
	private List<SimulationUpdatable> my_agents = new LinkedList<SimulationUpdatable>();

	/**
	 * Creates a new WP3ApplicationHost.
	 * 
	 * @param some_settings
	 *            the settings file.
	 */
	public SapereApplication(final Settings some_settings) {
		super(some_settings);
	}

	/**
	 * Sets up the advertisement service and eco-laws in all nodes.
	 */
	public final void setupAll() {
		setupEcoLaws();
		
		SapereAgent agentBond = new ChemotaxisManager(getHost().getName(), getNodeManager(), appID, getHost());
		new AgentChemoChecker(getHost().getName(), getNodeManager(), appID, getHost()).setInitialLSA();
		agentBond.setInitialLSA();
		
	}

	/**
	 * Adds an agent to the simulation update cycle.
	 * 
	 * @param an_updatableAgent
	 *            the agent to add to the update cycle.
	 */
	public final void addToUpdateCycle(
			final SimulationUpdatable an_updatableAgent) {
		my_agents.add(an_updatableAgent);
	}

	/**
	 * Sets up the eco-laws to be run in each node.
	 */
	private void setupEcoLaws() {
		final List<IEcoLaw> ecoLaws = new LinkedList<IEcoLaw>();
		ecoLaws.add(new Decay(getNodeManager()));
		ecoLaws.add(new Aggregation2(getNodeManager()));
		ecoLaws.add(new Bonding(getNodeManager()));
		ecoLaws.add(new Chemotaxis3(getNodeManager()));
		ecoLaws.add(selectSpreadingEcoLaw());
		getNodeManager().getEcoLawsEngine().replaceEcoLaws(ecoLaws);
	}

	/**
	 * Instantiates the correct spreading eco-law based on the properties file.
	 * 
	 * @return the spreading eco-law to use.
	 */
	private IEcoLaw selectSpreadingEcoLaw() {
		switch (SapereProperties.SPREADING_MODE) {
		case REGULAR:
			return new RegularSpreading(getNodeManager(),getHost());
		case LOCATION_BASED:
			return new LocationBasedSpreading(getNodeManager(), getHost());
		case PROBABALISTIC:
			return new ProbabilisticSpreading(getNodeManager(),getHost());
		case ADAPTIVE_PROBABALISITC:
			return new AdaptiveProbabilisticSpreading(getNodeManager(),getHost());
		case MIDDLEWARE_STANDARD:
			return new Propagation(getNodeManager());
		case SWITCHING:
			return new SwitchingSpreading(getNodeManager(), getHost());
		default:
			return new Propagation(getNodeManager());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final void onSimulationCycle() {
		globalUpdate();
		for (SimulationUpdatable agent : my_agents) {
			agent.onSimulationUpdate();
			
		}
	}

	public void globalUpdate() {
	}

}
