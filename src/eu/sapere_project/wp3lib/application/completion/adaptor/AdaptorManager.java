package eu.sapere_project.wp3lib.application.completion.adaptor;

import sapere.node.NodeManager;

public class AdaptorManager {

   private String name = null;

   private AdaptorImplementation i = null;

   private AdaptorService s = null;

   public AdaptorManager(String name, NodeManager an_opMng) {

      this.name = name;
      this.startManager(an_opMng);
   }

   private void startManager(NodeManager an_opMng) {

      s = new AdaptorService(name, an_opMng);
      i = new AdaptorImplementation(name, s, an_opMng);

      s.setInitialLSA(i);
   }

}
