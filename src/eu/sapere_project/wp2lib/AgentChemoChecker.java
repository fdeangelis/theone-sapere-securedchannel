// [Project Title]
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package eu.sapere_project.wp2lib;

import extension.SapereProperties;
import one.core.DTNHost;
import one.gui.playfield.GradientColor;
import one.gui.playfield.MapGraphic;
import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.lsa.interfaces.ILsa;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;
import saperedemo.Const;

/**
 * @author Jose Luis Fernandez-Marquez
 */
public class AgentChemoChecker extends SapereAgent {

   private final String my_nodeName;

   private final String my_value;
   
   private DTNHost host;

   /**
    * Creates a new agent.
    * @param name the name of the node
    * @param an_opMng the operation manager for the node.
    */
   public AgentChemoChecker(String name, NodeManager a_nodeManager, String a_value, DTNHost host) {
      super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
      my_nodeName = name;
      my_value = a_value;
      this.host = host;
   }

   /**
    * {@inheritDoc}
    */
   public void onBondAddedNotification(BondAddedEvent event) {
      // TODO Auto-generated method stub
	   //System.out.println("Node " + my_nodeName + " received the value");
	   if(SapereProperties.GRADIENTS_EVALUATION){	
		   if (event.getBondedLsa().getProperty("chemotaxis") != null)
		   {
			   String value = event.getBondedLsa().getProperty(Const.PROP_ENC_MEX).getValue().get(0);
			   this.host.hasChemo = true;
			   if (host.getName().equals("Node0") || host.getName().equals("Node25"))
				   return;
			   System.out.println(this.host.getName() + " chemotaxis value: " + value);
		   }	
	   }
   }

   /**
    * {@inheritDoc}
    */
   public void onBondRemovedNotification(BondRemovedEvent event) 
   {
	   //this.host.hasChemo = false;
   }

   /**
    * {@inheritDoc}
    */
   public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
      // TODO Auto-generated method stub

   }

   /**
    * {@inheritDoc}
    */
   public void onPropagationEvent(PropagationEvent event) {
   }

   /**
    * {@inheritDoc}
    */
   public void setInitialLSA() {
      //lsa.addProperty(new Property("NAME", my_nodeName));
      //lsa.addProperty(new Property("Example", "bonding-example"));
      //lsa.addProperty(new Property("a-bonding-keyword", my_value));
	   
	   if(SapereProperties.GRADIENTS_EVALUATION){
		   lsa.addSubDescription("d", new Property("chemotaxis", "*"));
	   }else{
		   lsa.addSubDescription("q", new Property("data-value", "*")); 
	   }
	   submitOperation();
   }

public void onReadNotification(ReadEvent readEvent) {
	// TODO Auto-generated method stub
	
}

@Override
public void onDecayedNotification(DecayedEvent event) {
	// TODO Auto-generated method stub
	
}
}
