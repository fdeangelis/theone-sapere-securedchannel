package eu.sapere_project.wp2lib.application;

import java.util.UUID;

import one.core.Application;
import one.core.Settings;
import one.core.SimClock;
import sapere.agent.SapereAgent;
import sapere.lsa.Id;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.node.lsaspace.Operation;
import saperedemo.AgentInjector;
import saperedemo.AgentReader;
import eu.sapere_project.wp3lib.application.SapereApplication;
import extension.SapereProperties;

/**
 * This class implements the secured channel example, that delegates on the
 * dynamic gradient library and chemotaxis library.
 * 
 * Francesco de Angelis
 */
public final class SecuredChannelExample extends SapereApplication {
	
	/**
	 * Create a new secured channel example.
	 * 
	 * @param some_settings
	 *            the simulation settings.
	 */
	public SecuredChannelExample(final Settings some_settings) {
		super(some_settings);
	}

	public Lsa my_lsa;

	private boolean injectWhenDeleted = false;

	/**
	 * Creates an initial Lsa in node 0.
	 */
	public void setupNode0() 
	{	
		AgentInjector agent = new AgentInjector("AgentInjector","Node0", getNodeManager());
		agent.setInitialLSA();
	}
	
	/**
	 * An example of a chemotaxis LSA - it follows the gradient established in Node0
	 */
	public void setupNode25() 
	{
		AgentReader agent = new AgentReader("AgentReader","Node25", getNodeManager());
		agent.setInitialLSA();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Application replicate() {
		return new SecuredChannelExample(getSettings());
	}
}
