/**
 * 
 */
package sapere.lsa.exception;

/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class UnresolvedPropertyNameException extends Exception {}
