/**
 * 
 */
package sapere.lsa.exception;

/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class MalformedPropertyException  extends RuntimeException {}
