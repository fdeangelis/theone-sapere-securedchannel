/**
 * 
 */
package sapere.lsa.exception;

/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class IllegalSyntheticPropertyException  extends RuntimeException {}
