/**
 * 
 */
package sapere.lsa.values;

/**
 * Well-known Properties
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public enum PropertyName {
	
	DIFFUSION_OP("diffusion_op"), 
	
	AGGREGATION_OP ("aggregation_op"), 
	
	FIELD_VALUE ("field_value"),
	
	SOURCE ("source"), 

	PREVIOUS ("previous"), 
	
	DESTINATION ("destination"),
	
	ACTIVE_PROPAGATION("active_propagation"),
	
	// Used for the DECAY ECO-LAW, workes as a TIME-TO-LIVE
	DECAY ("decay"); 
	
	private PropertyName (final String text) {
        this.text = text;
    }

    private final String text;

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return text;
    }

	
	public static boolean isDecay(String s){
		return s.toLowerCase().equals(DECAY.toString());
	};
	
	private static final int decayDecrement = 1;
	
	public static boolean isDecayValue (String s) {
        
        try {

            Integer.parseInt(s);
        
        } catch (NumberFormatException ex) {
            return false;
        }
        
        return true;
    }
	
	public static Integer getDecayValue(String s){
		
		Integer n = null;
		
		try {

            n = Integer.parseInt(s);
        
        } catch (NumberFormatException ex) {
            
        }
		
		return n;
	}
	
	public static Integer decrement (Integer n){		
		return n - decayDecrement;
	}
	
	public static boolean isExpired(Integer n){
		return n.intValue() == 0;
	}
	
}
