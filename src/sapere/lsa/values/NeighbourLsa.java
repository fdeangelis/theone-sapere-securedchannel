package sapere.lsa.values;


public enum NeighbourLsa {
		
		NEIGHBOUR ("neighbour"), 
		
		IP_ADDRESS ("ipAddress");
		
		  private String text;

		  NeighbourLsa (String text) {
		    this.text = text;
		  }
		  
		  @Override
		  public String toString() {
			    return this.text;
			  }

		  public String getText() {
		    return this.text;
		  }

}
