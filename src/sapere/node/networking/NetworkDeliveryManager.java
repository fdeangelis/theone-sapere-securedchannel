package sapere.node.networking;

import java.util.UUID;
import java.util.logging.Logger;

import one.core.Connection;
import one.core.DTNHost;
import one.core.Message;
import one.core.SimClock;
import sapere.lsa.Lsa;

import com.google.common.base.Optional;

import extension.AbstractSapereHost;
import extension.SapereProperties;

public class NetworkDeliveryManager implements INetworkDeliveryManager {

   /**
    * The class logger.
    */
   private static final Logger LOGGER = Logger.getLogger("sapere.node.networking.NetworkDeliveryManager");

   /**
    * The modes of transport.
    */
   private enum Mode {
      SPREAD, DIRECT
   };

   /**
    * The local host.
    */
   final DTNHost my_host;

   /**
    * Creates the delivery manager for a given host.
    * @param a_host
    */
   public NetworkDeliveryManager(final DTNHost a_host) {
      my_host = a_host;
   }

   /**
    * {@inheritDoc}
    */
   public boolean doSpread(final Lsa an_lsa) {
      LOGGER.finest("Spreading an LSA from " + my_host.getName());
      
      if(SapereProperties.BROADCAST){
    	  SapereProperties.MESSAGE_COUNTER++;
      }
      for (Connection connection : my_host.getConnections()) {
         final DTNHost neighbour = connection.getOtherNode(my_host);
         send(neighbour, an_lsa, Mode.SPREAD);
      }
      return true;
   }

   /**
    * {@inheritDoc}
    */
   public boolean sendDirect(String a_destinationName, Lsa an_lsa) {
      boolean successful = false;
      Optional<DTNHost> destinationHost = Optional.absent();
      for (Connection connection : my_host.getConnections()) {
         final DTNHost neighbour = connection.getOtherNode(my_host);
         if (neighbour.getName().equals(a_destinationName)) {
            destinationHost = Optional.fromNullable(neighbour);
            break;
         }
      }
      // If null, node is not a neighbour. Otherwise, send the message.
      if (destinationHost.isPresent()) {
         send(destinationHost.get(), an_lsa, Mode.DIRECT);
         successful = true;
         LOGGER.finest("Sent an LSA from " + my_host.getName() + " to " + a_destinationName);
      } else {
         LOGGER.warning("Failed to send an LSA from " + my_host.getName() + " to " + a_destinationName
               + "(Nodes are not neighbours)");
      }
      return successful;
   }

   /**
    * Sends an LSA direct to a destination.
    * @param a_destination the destination node.
    * @param an_lsa the LSA to send.
    * @param the mode that triggered the message.
    */
   private void send(final DTNHost a_destination, final Lsa an_lsa, final Mode a_mode) {
      final String msgID = generateMessageId(a_destination, a_mode);
      final Message msg = new Message(my_host, a_destination, msgID, 0);
      msg.addProperty("lsa", an_lsa);
      msg.setAppID(AbstractSapereHost.APP_ID);
      my_host.createNewMessage(msg);
   }

   /**
    * Generates a unique message ID for a message with a given destination.
    * @param a_destination the destination node.
    * @param a_mode the mode of propagation.
    * @return a unique message id.
    */
   public String generateMessageId(final DTNHost a_destination, final Mode a_mode) {
      return a_mode.name() + ":" + my_host.getName() + "-" + a_destination.getName() + ":" + SimClock.getTime() + "-"
            + UUID.randomUUID();
   }

}
