package sapere.node.networking;

// import eu.sapere.middleware.agent.BasicSapereAgent;
import java.util.logging.Logger;

import one.core.DTNHost;
import sapere.lsa.Id;
import sapere.lsa.Lsa;
import sapere.lsa.SubDescription;
import sapere.node.lsaspace.LsaReceived;
import sapere.node.lsaspace.Operation;
import sapere.node.lsaspace.OperationManager;

public class NetworkReceiverManager implements INetworkReceiverManager, LsaReceived {

   /**
    * The local operation manager.
    */
   private final OperationManager my_operationManager;

   private static final Logger LOGGER = Logger.getLogger("sapere.node.networking");

   /**
    * The local host.
    */
   final DTNHost my_host;

   public NetworkReceiverManager(final DTNHost a_host, final OperationManager an_operationManager) {
      my_host = a_host;
      my_operationManager = an_operationManager;
   }

   public void doInject(Lsa receivedLsa) {
      // Reset SubDescriptionId before injecting the LSA
      SubDescription s[] = receivedLsa.getSubDescriptions();
      if (s != null)
         for (int i = 0; i < s.length; i++) {
            s[i].setId(new Id(s[i].getId().toString().substring(s[i].getId().toString().lastIndexOf("#") + 1)));
         }

      Operation op = new Operation().injectOperation(receivedLsa, "From_Remote", null);

      my_operationManager.queueOperation(op);
      LOGGER.finest(" Host: " + my_host.getName() + " received a LSA: " + receivedLsa);

   }

   public void onLsaReceived(Lsa lsaReceived) {
      doInject(lsaReceived);
   }

}
