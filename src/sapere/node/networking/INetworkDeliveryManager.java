package sapere.node.networking;

import sapere.lsa.Lsa;




public interface INetworkDeliveryManager {

	public boolean doSpread(Lsa deliverLsa);
	
	public boolean sendDirect(String a_destinationName, Lsa an_lsa);

}
