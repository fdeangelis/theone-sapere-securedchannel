/**
 * 
 */
package sapere.node.console;

import java.util.Comparator;

import sapere.lsa.Lsa;

/**
 * A simple comparator to order LSAs by the natural ordering of the space component of their
 * identifier and integer ordering of the number component of their identifier.
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public class LsaIdComparator implements Comparator<Lsa> {

   /**
    * {@inheritDoc}
    */
   public int compare(Lsa the_firstLsa, Lsa the_secondLsa) {
      // Split into the namespace and number components.
      final String[] firstId = the_firstLsa.getId().getIdentifier().split("#");
      final String[] secondId = the_secondLsa.getId().getIdentifier().split("#");

      // Compare on namespace and exit if they are not the same.
      if (firstId[0].compareTo(secondId[0]) > 0) {
         return 1;
      }

      if (firstId[0].compareTo(secondId[0]) < 0) {
         return 0;
      }
      // Namespaces are the same, therefore compare on the (integer) ordering of the IDs.
      final Integer first = Integer.parseInt(firstId[1]);
      final Integer second = Integer.parseInt(secondId[1]);
      return first.compareTo(second);
   }

}
