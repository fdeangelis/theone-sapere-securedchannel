package sapere.node.console;

import java.awt.GridLayout;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import sapere.lsa.Lsa;

/**
 * Class that represent a monitor for the Space content
 * @author Marco Santarelli (UNIBO)
 * @author Gabriella Castelli (UNIMORE)
 */
public class SpaceConsoleImpl implements SpaceConsole {

   JTextArea lsastext, reactstext;

   private JFrame frame = null;

   private String nodeName;

   /**
    * Creates a new SpaceMonitor
    */
   public SpaceConsoleImpl(String nodeName) {
      this.nodeName = nodeName;
   }

   public SpaceConsoleImpl() {
   }

   public void startConsole(String nodeName) {
      this.nodeName = nodeName;
      initConsole();
   }

   private void initConsole() {
      frame = new JFrame("Node: " + nodeName);

      JPanel main = new JPanel();
      main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));

      lsastext = new JTextArea();
      lsastext.setEditable(false);
      reactstext = new JTextArea();
      reactstext.setEditable(false);
      JLabel lsaslabel = new JLabel("LSA Space content");

      JScrollPane lsaspane = new JScrollPane(lsastext);
      JScrollPane reactspane = new JScrollPane(reactstext);

      main.add(lsaslabel);

      main.add(lsaspane);

      frame.setLayout(new GridLayout(1, 1));
      frame.getContentPane().add(main);
      frame.pack();

      frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      frame.setSize(510, 250);
      frame.setResizable(true);
      frame.setVisible(false);
   }

   /**
    * Updates the Monitor, printing the given list of LSA
    * @param list the list of LSA to show
    */
   public void update(final Lsa[] list) {
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            String tmp = "";
            // Edit 18/10/12 - Graeme: support for ordered lsa display.
            try {
               Arrays.sort(list, new LsaIdComparator());
            } catch (Exception e) {
               System.err
                     .println("Exception while attempting to sort LSAs for GUI display. Perhaps ID format is unexpected?"
                           + " See the SpaceConsoleImpl and LsaIdComparator classes.");
            }

            for (Lsa e : list) {
               tmp += e.toString() + "\n";
            }
            lsastext.setText(tmp);
         }

      });
   }

   public void setConsoleVisible() {
      this.frame.setVisible(true);

   }

   public void updateGUI(Lsa[] list) {
      // TODO Auto-generated method stub

   }

}
