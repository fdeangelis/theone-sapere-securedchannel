/**
 * 
 */
package sapere.node.lsaspace;

import java.io.Serializable;

import sapere.lsa.Id;
import sapere.lsa.Lsa;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class Operation implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5118578211958268938L;
	private Lsa lsa = null;
	private Id lsaId = null;
	private OperationType opType = null;
	private String requestingId = null;
	
	private Agent requestingAgent = null;
	
	

	
	/**
	 * @return the lsa
	 */
	public Lsa getLsa() {
		return lsa;
	}

	/**
	 * @param lsa the lsa to set
	 */
	public void setLsa(Lsa lsa) {
		this.lsa = lsa;
	}

	/**
	 * @return the lsaId
	 */
	public Id getLsaId() {
		return lsaId;
	}

	/**
	 * @param lsaId the lsaId to set
	 */
	public void setLsaId(Id lsaId) {
		this.lsaId = lsaId;
		this.lsa.setId(lsaId);
	}

	/**
	 * @return the opType
	 */
	public OperationType getOpType() {
		return opType;
	}

	/**
	 * @param opType the opType to set
	 */
	public void setOpType(OperationType opType) {
		this.opType = opType;
	}

	/**
	 * @return the requestingId
	 */
	public String getRequestingId() {
		return requestingId;
	}
	
	/**
	 * @return the requesting Agent
	 */
	public Agent getRequestingAgent(){
		return requestingAgent;
	}

	/**
	 * @param requestingId the requestingId to set
	 */
	public void setRequestingId(String requestingId) {
		this.requestingId = requestingId;
	}

	public Operation injectOperation(Lsa lsa, String requestingId, Agent requestingAgent){
		
		this.opType = OperationType.Inject;
		this.lsaId = null; // To be decided where should be fixed
		this.lsa = lsa.getCopy();
		this.requestingId = new String(requestingId);
		this.requestingAgent = requestingAgent;
		
		return this;
		}
	
	public Operation updateOperation(Lsa lsa, Id lsaId, String requestingId, Agent requestingAgent){
		this.opType = OperationType.Update;
		this.lsaId = new Id(lsaId.toString());
		this.lsa = lsa.getCopy();
		this.requestingId = new String(requestingId);
		this.requestingAgent = requestingAgent;
		
		return this;
	}
	
	public Operation updateBondOperation(Lsa lsa, Id lsaId, String requestingId){
		this.opType = OperationType.BondUpdate;
		this.lsaId = new Id(lsaId.toString());
		this.lsa = lsa.getCopy();
		this.requestingId = new String(requestingId);
		
		return this;
	}
	
	public Operation updateParametrizedOperation(Lsa lsa, Id lsaId, String requestingId){
		this.opType = OperationType.UpdateParametrized;
		this.lsaId = new Id(lsaId.toString());
		this.lsa = lsa.getCopy();
		this.requestingId = new String(requestingId);
		
		return this;
	}
	
	public Operation updateBondParametrizedOperation(Lsa lsa, Id lsaId, String requestingId){
		this.opType = OperationType.BondUpdateParametrized;
		this.lsaId = new Id(lsaId.toString());
		this.lsa = lsa.getCopy();
		this.requestingId = new String(requestingId);
		
		return this;
	}
	
	public Operation removeOperation(Id lsaId, String requestingId){
		
		this.opType = OperationType.Remove;
		this.lsaId = new Id(lsaId.toString());
		this.requestingId = new String(requestingId);

		return this;
	}
	
	public Operation readOperation(Id lsaId, String requestingId, Agent requestingAgent){
		
		this.opType = OperationType.Read;
		this.lsaId = new Id(lsaId.toString());
		this.requestingId = new String(requestingId);
		this.requestingAgent = requestingAgent;
		
		return this;
	}
	
	
	
	public String toString(){
		String res = "operation[type="+opType;
		if(this.lsaId != null)
			res+=",lsaId="+this.lsaId;
		if(lsa != null)
			res+=",lsa="+lsa;
		if (requestingId != null)
			res+=",requestedBy="+requestingId;
		return res+="]";
	}
	
	

}
