/**
 * 
 */
package sapere.node.lsaspace;

import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import one.core.SimClock;
import sapere.lsa.Id;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.SubDescription;
import sapere.lsa.SyntheticProperty;
import sapere.lsa.values.SyntheticPropertyName;
import sapere.node.console.SpaceConsole;
import sapere.node.notifier.INotifier;
import sapere.node.notifier.event.AbstractSapereEvent;
import sapere.node.notifier.event.AggregationRemovedEvent;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.LsaExpiredEvent;
import sapere.node.notifier.event.ReadEvent;

import com.google.common.base.Optional;

// import sapere.node.notifier.event.DecayedEvent;

/**
 * @author Gabriella Castelli (UNIMORE)
 */
public class Space {

	private HashMap<String, Lsa> space = null;

	private String name = null;

	private static long nextId = 0L;

	private INotifier notifier = null;

	private SpaceConsole console = null;

	public Space(String name, INotifier notifier, SpaceConsole console) {
		space = new HashMap<String, Lsa>();
		this.name = name;
		this.notifier = notifier;
		this.console = console;
	}

	private void updateConsole() {
		if (console != null)
			console.update(getAllLsa());
	}

	/* inject the copy of an LSA into the LSA-space */
	public void inject(Lsa incomingLsa, String creatorId) {

		// ** Updated to support new spreading ecolaws **
		// ** Code below the if statement is unchanged **
		// ** Applies only to LSAs with a uuid property **
		if (incomingLsa.getSingleValue("uuid").isPresent()) {
			// Search for a local LSA with the same UUID
			Optional<Lsa> localLsa = getLsaWithUUID(incomingLsa.getSingleValue(
					"uuid").get());

			// Update the local copy and discard the incoming copy.
			if (localLsa.isPresent()) {
				updateLocalLsaWithIncomingCopy(incomingLsa, localLsa.get());
				updateConsole();
				return;
			}
		}
		// Fall through to the original standard code below.

		Lsa copy = incomingLsa.getCopy();
		// add Synthetic Properties
		copy.addProperty(new SyntheticProperty(
				SyntheticPropertyName.CREATOR_ID, new String(creatorId)));
		copy.addProperty(new SyntheticProperty(
				SyntheticPropertyName.CREATION_TIME, Double.toString(SimClock
						.getTime())));
		copy.addProperty(new SyntheticProperty(
				SyntheticPropertyName.LAST_MODIFIED, Double.toString(SimClock
						.getTime())));
		copy.addProperty(new SyntheticProperty(SyntheticPropertyName.LOCATION,
				"local"));

		space.put(incomingLsa.getId().toString(), copy);
		updateConsole();

	}

	/**
	 * Updates the local LSA with information from the incoming LSA.
	 * 
	 * @param the_incomingLsa
	 *            the incoming LSA.
	 * @param theLocalLsa
	 *            the local LSA.
	 */
	private void updateLocalLsaWithIncomingCopy(Lsa the_incomingLsa,
			Lsa theLocalLsa) {
		
		// If this LSA is a gradient
		if (the_incomingLsa.hasProperty("hopsTravelled") && theLocalLsa.hasProperty("hopsTravelled")) {
			// Select the one with the lower hop count.
			Integer incomingHops = Integer.parseInt(the_incomingLsa.getSingleValue("hopsTravelled").get());
			Integer localHops = Integer.parseInt(theLocalLsa.getSingleValue("hopsTravelled").get());
			Lsa withLowerHopCount = localHops <= incomingHops ? theLocalLsa : the_incomingLsa; 
			
			//Now update properties.
			theLocalLsa.addProperty(new Property("hopsTravelled",withLowerHopCount.getSingleValue("hopsTravelled").get()));
			theLocalLsa.addProperty(new Property("previous",withLowerHopCount.getSingleValue("previous").get()));
		}
		
		
		
		if (the_incomingLsa.hasProperty("spread_from")) {
			// Update the local LSA adding the new spread from coordinates.
			if (theLocalLsa.hasProperty("spread_from")) {
				theLocalLsa
						.getProperty("spread_from")
						.getValue()
						.add(the_incomingLsa.getSingleValue("spread_from")
								.get());
			} else {
				theLocalLsa.addProperty(new Property("spread_from",
						the_incomingLsa.getSingleValue("spread_from").get()));
			}
		}
	}

	/**
	 * Searched the local space for an LSA with the given UUID property
	 * 
	 * @param singleValue
	 * @return
	 */
	private Optional<Lsa> getLsaWithUUID(String a_uuid) {
		// Search for an LSA with the unique ID
		for (final Lsa lsa : getAllLsa()) {
			if (lsa.hasProperty("uuid")
					&& a_uuid.equals(lsa.getSingleValue("uuid").get())) {
				return Optional.of(lsa);
			}
		}
		return Optional.absent();
	}

	public void update(Id lsaId, Lsa lsa, String creatorId) {
		update(lsaId, lsa, creatorId, true, true);
	}

	public void update(Id lsaId, Lsa lsa, String creatorId,
			boolean destroyBonds, boolean generateEvents) {
		Lsa copy = lsa.getCopy();
		Lsa oldLsa = space.get(lsaId.toString());

		boolean generatedBond = false;

		if (space.containsKey(lsaId.toString()))
			if (space.get(lsaId.toString()).hasSyntheticProperty(
					SyntheticPropertyName.CREATOR_ID))
				if (space.get(lsaId.toString())
						.getSyntheticProperty(SyntheticPropertyName.CREATOR_ID)
						.getValue().elementAt(0).equals(creatorId)
						|| creatorId.equals(EcoLawsEngine.ECO_LAWS_AGENT)
						|| creatorId.equals(EcoLawsEngine.ECO_LAWS_DECAY)) {

					copy.setId(new Id(lsaId.toString()));

					String currentTime = "" + new Date().getTime();
					copy.addProperty(new SyntheticProperty(
							SyntheticPropertyName.CREATOR_ID, new String(space
									.get(lsaId.toString())
									.getSyntheticProperty(
											SyntheticPropertyName.CREATOR_ID)
									.getValue().elementAt(0))));
					copy.addProperty((SyntheticProperty) oldLsa
							.getSyntheticProperty(SyntheticPropertyName.CREATION_TIME));
					copy.addProperty(new SyntheticProperty(
							SyntheticPropertyName.LAST_MODIFIED, Double
									.toString(SimClock.getTime())));
					copy.addProperty(new SyntheticProperty(
							SyntheticPropertyName.LOCATION, "local"));
					if (creatorId.equals(EcoLawsEngine.ECO_LAWS_AGENT)) {
						if (lsa.hasBonds())
							for (int i = 0; i < lsa
									.getSyntheticProperty(
											SyntheticPropertyName.BONDS)
									.getValue().size(); i++)
								copy.addBond(lsa
										.getSyntheticProperty(
												SyntheticPropertyName.BONDS)
										.getValue().elementAt(i));
					}

					else {
						if (oldLsa.hasBonds())
							for (int i = 0; i < oldLsa
									.getSyntheticProperty(
											SyntheticPropertyName.BONDS)
									.getValue().size(); i++)
								copy.addBond(oldLsa
										.getSyntheticProperty(
												SyntheticPropertyName.BONDS)
										.getValue().elementAt(i));

						SubDescription sd[] = copy.getSubDescriptions();
						if (sd != null)
							for (int i = 0; i < sd.length; i++) {
								SubDescription oldSd = oldLsa
										.getSubDescriptionById(sd[i].getId()
												.toString());
								if (oldSd != null)
									if (oldSd.hasBonds()) {
										Vector<String> oldBonds = oldSd
												.getSyntheticProperty(
														SyntheticPropertyName.BONDS)
												.getValue();
										for (int j = 0; j < oldBonds.size(); j++)
											sd[i].addBond(oldBonds.elementAt(j));

									}
							}
					}

					space.put(copy.getId().toString(), copy);

					if (destroyBonds)
						destroyBonds();

					// Delete to generate BondedLsaUpdateEvent also for
					// synthetic props
					/*
					 * if (generateEvents) {SapereEvent event = new
					 * BondedLsaUpdateEvent(copy);
					 * event.setRequiringAgent(null); notifier.publish(event);}
					 */

					String subjectId = copy.getId().toString();

					// if(destroyBonds)
					// destroyBonds(this.getAllLsa(), this);

					// Update the lsa
					lsa = space.get(subjectId);

					// Generate Events
					if (lsa.hasBonds())

						for (int i = 0; i < lsa
								.getSyntheticProperty(
										SyntheticPropertyName.BONDS).getValue()
								.size(); i++) {

							if (!oldLsa.hasBondWith(new Id(lsa
									.getSyntheticProperty(
											SyntheticPropertyName.BONDS)
									.getValue().elementAt(i)))) {

								Lsa bondedLsa = getLsa(lsa
										.getSyntheticProperty(
												SyntheticPropertyName.BONDS)
										.getValue().elementAt(i));
								AbstractSapereEvent event = new BondAddedEvent(
										copy.getCopy(),
										lsa.getSyntheticProperty(
												SyntheticPropertyName.BONDS)
												.getValue().elementAt(i),
										bondedLsa);
								event.setRequiringAgent(new String(
										space.get(lsaId.toString())
												.getSyntheticProperty(
														SyntheticPropertyName.CREATOR_ID)
												.getValue().elementAt(0)));

								notifier.publish(event);

								generatedBond = true;

							}
						} // end for

					if (lsa.hasSubDescriptions()) {
						SubDescription sd[] = lsa.getSubDescriptions();
						for (int i = 0; i < sd.length; i++) {
							if (sd[i].hasBonds()) {
								Vector<String> bonds = sd[i]
										.getSyntheticProperty(
												SyntheticPropertyName.BONDS)
										.getValue();
								for (int j = 0; j < bonds.size(); j++) {
									if (!oldLsa.hasBondWith(new Id(bonds
											.elementAt(j)))) {

										Lsa bondedLsa = getLsa(bonds
												.elementAt(j));

										AbstractSapereEvent event = new BondAddedEvent(
												copy.getCopy(),
												bonds.elementAt(j), bondedLsa);
										event.setRequiringAgent(new String(
												space.get(lsaId.toString())
														.getSyntheticProperty(
																SyntheticPropertyName.CREATOR_ID)
														.getValue()
														.elementAt(0)));
										notifier.publish(event);

										generatedBond = true;

									}

								}
							}
						}
					}

				}

		// fires the events also if the Bond has just been created
		{
			AbstractSapereEvent event = new BondedLsaUpdateEvent(copy);
			event.setRequiringAgent(null);
			notifier.publish(event);
		}

		updateConsole();
	}

	public void remove(Id lsaId, String creatorId) {

		if (space.containsKey(lsaId.toString()))
			if (space.get(lsaId.toString()).hasSyntheticProperty(
					SyntheticPropertyName.CREATOR_ID))
				if (space.get(lsaId.toString())
						.getSyntheticProperty(SyntheticPropertyName.CREATOR_ID)
						.getValue().elementAt(0).equals(creatorId)
						|| creatorId.equals(EcoLawsEngine.ECO_LAWS_AGENT)
						|| creatorId.equals(EcoLawsEngine.ECO_LAWS_DECAY)
						|| creatorId.equals(EcoLawsEngine.ECO_LAWS_AGGREGATION)
						|| creatorId.equals(EcoLawsEngine.ECO_LAWS_PROPAGATION)) {

					Lsa oldLsa = space.get(lsaId.toString());
					space.remove(lsaId.toString());

					if (creatorId.equals(EcoLawsEngine.ECO_LAWS_DECAY)) {
						// Fire event LsaExpired
						AbstractSapereEvent event = new LsaExpiredEvent(oldLsa);
						event.setRequiringAgent(oldLsa
								.getSyntheticProperty(
										SyntheticPropertyName.CREATOR_ID)
								.getValue().elementAt(0).toString());
						notifier.publish(event);
					}

					if (creatorId.equals(EcoLawsEngine.ECO_LAWS_AGGREGATION)) {

						AbstractSapereEvent event = new AggregationRemovedEvent(
								oldLsa);
						event.setRequiringAgent(oldLsa
								.getSyntheticProperty(
										SyntheticPropertyName.CREATOR_ID)
								.getValue().elementAt(0).toString());

						notifier.publish(event);
					}
				}

		destroyBonds();
		updateConsole();
	}

	public void read(Id targetId, String requestingId) {
		Lsa ret = null;

		if (space.containsKey(targetId.toString())) {
			Lsa target = space.get(targetId.toString());

			ret = target.getCopy();
		}

		// TO DO: check a chain of bonds instead of a single bond

		/*
		 * String targetCreator =
		 * target.getSyntheticProperty(SyntheticPropertyName
		 * .CREATOR_ID).getValue().firstElement(); if
		 * (targetCreator.equals(requestingId)){ ret = target.getCopy(); } else{
		 * Iterator<String> iterator = space.keySet().iterator(); while
		 * (iterator.hasNext()){ String key = (String) iterator.next(); Lsa lsa
		 * = space.get(key); String creatorId =
		 * lsa.getSyntheticProperty(SyntheticPropertyName
		 * .CREATOR_ID).getValue().firstElement(); if
		 * (creatorId.equals(requestingId)) if (lsa.hasBondWith(targetId)){ ret
		 * = target.getCopy(); break;} } } }
		 */

		if (ret != null) {
			AbstractSapereEvent event = new ReadEvent(ret);
			event.setRequiringAgent(requestingId);
			notifier.publish(event);
		}

		// updateConsole();
	}

	public Lsa getLsa(String key) {
		Id id = new Id(key);
		Lsa ret = null;

		if (!id.isSdId(id)) {
			if (space.containsKey(key.substring(0, key.lastIndexOf("#")))) {
				ret = space.get(key.substring(0, key.lastIndexOf("#")));
			}
		} else {
			if (space.containsKey(key)) {
				ret = space.get(key);
			}
		}
		return ret;
	}

	public Id getFreshId() {
		while (space.containsKey(name + "#" + nextId)) {
			nextId++;
		}
		return new Id(name + "#" + nextId++).getCopy();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {

		return space.values().toString();
	}

	public Lsa[] getAllLsa() {
		Lsa[] array = new Lsa[space.values().size()];

		return space.values().toArray(array);
	}

	private static boolean isLsaId(String s) {
		if (s.indexOf("#") != s.lastIndexOf("#"))
			return false;
		return true;
	}

	private void destroyBonds() {
		for (Lsa lsa : getAllLsa()) {
			if (lsa.hasBonds()) {
				Vector<String> activeBonds = lsa.getSyntheticProperty(
						SyntheticPropertyName.BONDS).getValue();
				Vector<String> removeBonds = new Vector<String>(); // Bonds to
																	// be
																	// removed
				for (String bond : activeBonds) {
					// Differentiate based on whether bond is with an LSA or
					// sub-description.
					if (isLsaId(bond)) {
						if (!space.containsKey(bond)) {
							removeBonds.add(bond);
						} else {
							if (!lsa.matches(getLsa(bond))
									&& !getLsa(bond).matches(lsa)) {
								removeBonds.add(bond);
							}
						}
					} else {
						String LsaId = bond.substring(0, bond.lastIndexOf("#"));
						if (getLsa(LsaId) == null) {
							removeBonds.add(bond);
						} else if (!lsa.matches(getLsa(LsaId)
								.getSubDescriptionById(bond))
								&& !getLsa(LsaId).getSubDescriptionById(bond)
										.hasBond(lsa.getId().toString()))
							removeBonds.add(bond);
					}
				}

				for (String removeBond : removeBonds) {
					lsa.removeBond(removeBond);
					AbstractSapereEvent event = new BondRemovedEvent(
							lsa.getCopy(), removeBond);
					event.setRequiringAgent(lsa
							.getCopy()
							.getSyntheticProperty(
									SyntheticPropertyName.CREATOR_ID)
							.getValue().elementAt(0));
					notifier.publish(event);

					// try to remove in the other
					Lsa exBondedLsa = getLsa(removeBond);
					if (exBondedLsa != null)
						if (exBondedLsa.hasBondWith(lsa.getId())) {
							exBondedLsa.removeBond(lsa.getId().toString());

							AbstractSapereEvent event2 = new BondRemovedEvent(
									exBondedLsa.getCopy(), lsa.getId()
											.toString());
							event2.setRequiringAgent(exBondedLsa
									.getCopy()
									.getSyntheticProperty(
											SyntheticPropertyName.CREATOR_ID)
									.getValue().elementAt(0));
							notifier.publish(event2);
						}
				}
			}
			// Remove subdescription bonds
			for (SubDescription subDesc : lsa.getSubDescriptions()) {
				removeSubBonds(lsa, subDesc);
			}

		}

	}

	// TO DO: check events
	public void removeSubBonds(Lsa an_lsa, SubDescription a_subDesc) {
		if (a_subDesc.hasBonds()) {
			Vector<String> activeSubBonds = a_subDesc.getSyntheticProperty(
					SyntheticPropertyName.BONDS).getValue();
			Vector<String> removeSubBonds = new Vector<String>(); // Bonds to be
																	// removed
			for (String subBond : activeSubBonds) {
				if (isLsaId(subBond)) {
					if (!a_subDesc.matches(getLsa(subBond)))
						removeSubBonds.add(subBond);
				} else {
					String LsaId = subBond.substring(0,
							subBond.lastIndexOf("#"));
					// System.err.println(SimClock.getIntTime() +
					// ":  "+a_subDesc.getId() + " "+getLsa(LsaId) + " " + " " +
					// getLsa(LsaId).getSubDescriptionById(subBond));
					
					SubDescription bondedSubdescription = getLsa(LsaId)
							.getSubDescriptionById(subBond);
					if (bondedSubdescription == null
							|| !a_subDesc.matches(bondedSubdescription))
						removeSubBonds.add(subBond);
					
					
				}
			}

			for (String removeSubBond : removeSubBonds) {
				a_subDesc.removeBond(removeSubBond);
				AbstractSapereEvent event = new BondRemovedEvent(
						an_lsa.getCopy(), removeSubBond);
				event.setRequiringAgent(an_lsa.getCopy()
						.getSyntheticProperty(SyntheticPropertyName.CREATOR_ID)
						.getValue().elementAt(0));
				notifier.publish(event);
			}

		}
	}
}
