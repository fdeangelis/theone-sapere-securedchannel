/**
 * 
 */
package sapere.node.lsaspace;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import com.google.common.collect.Lists;

import extension.SapereProperties;

import one.core.Settings;
import one.core.SimClock;
import sapere.node.NodeManager;
import sapere.node.lsaspace.ecolaws.Aggregation;
import sapere.node.lsaspace.ecolaws.Bonding;
import sapere.node.lsaspace.ecolaws.Decay;
import sapere.node.lsaspace.ecolaws.IEcoLaw;
import sapere.node.lsaspace.ecolaws.Propagation;

/**
 * @author Gabriella Castelli (UNIMORE)
 * @author Graeme Stevenson (STA)
 */
public class EcoLawsEngine implements Runnable {

   /**
    * The default frequency for firing eco-laws if not specified in the property file.
    */
   private static final int DEFAULT_FIRING_FREQUENCY = 1;

   /**
    * The default number of cycles between triggering the eco-laws.
    */
   private long my_defaultFrequency = DEFAULT_FIRING_FREQUENCY;

   /**
    * The number of cycles between triggering the eco-laws that involve propagation.
    */
   private final long my_spreadFrequency;

   /**
    * The number of cycles between triggering the eco-laws that involve direct propagation.
    */
   private final long my_directPropagationFrequency;

   /**
    * The local sim cycle counter
    */
   private long my_cycleCounter = 0;

   public static final String ECO_LAWS_AGENT = "EcoLawsAgent";

   public static final String ECO_LAWS_DECAY = "EcoLawsDecay";

   public static final String ECO_LAWS_AGGREGATION = "EcoLawsAggregation";

   public static final String ECO_LAWS_PROPAGATION = "EcoLawsPropagation";

   private Settings my_settings;

   private static final Logger LOGGER = Logger.getLogger("sapere.node.lsaspace");

   private final List<IEcoLaw> my_ecoLaws = Lists.newLinkedList();

   public EcoLawsEngine(Space space, NodeManager a_nodeManager, Settings settings) {
      my_settings = settings;
      my_defaultFrequency = loadEcoLawFrequency("EcoLawsEngine.FiringFrequency");
      my_spreadFrequency = loadEcoLawFrequency("EcoLawsEngine.SpreadingFrequency");
      my_directPropagationFrequency = loadEcoLawFrequency("EcoLawsEngine.DirectPropagationFrequency");

      // Add the eco-laws in execution order.
      my_ecoLaws.add(new Decay(a_nodeManager));
      my_ecoLaws.add(new Aggregation(a_nodeManager));
      my_ecoLaws.add(new Bonding(a_nodeManager));
      my_ecoLaws.add(new Propagation(a_nodeManager));

   }

   /**
    * Loads an eco-law frequency from the simulator property file.
    * @param property the name of the property to load.
    * @return the frequency specified in the property file, or the default value if not found.
    */
   public long loadEcoLawFrequency(String property) {
      if (my_settings.contains(property)) {
         return Long.parseLong(my_settings.getSetting(property));
      } else {
         LOGGER.warning("No frequency specified in the settings file (<App>." + property + "). Defaulting to "
               + my_defaultFrequency + " cycle(s).");
         return my_defaultFrequency;
      }
   }

   public void run() {
      // Increment the execution cycle counter.
      my_cycleCounter++;
      LOGGER.finest("Starting EcoLawsManager cycle " + my_cycleCounter);
      // Invoke all the eco laws; invoke spreading/direct propagation laws according to the
      // frequency parameter.
//      for (IEcoLaw law : my_ecoLaws) {
//         if ((law.doesSpread() && my_cycleCounter % my_spreadFrequency == 0)
//               || (law.doesDirectPropagation() && my_cycleCounter % my_directPropagationFrequency == 0)
//               || (!law.doesSpread() && !law.doesDirectPropagation() && my_cycleCounter % my_defaultFrequency == 0)) {
//            law.invoke();
//            LOGGER.finer(law.getName() + " Eco-law executed");
//         }
//      }
      
      if(SapereProperties.CYCLE_COUNTER % SapereProperties.ECOLAW_FREQ ==0){
    	  for (IEcoLaw law : my_ecoLaws) {
    		  law.invoke();
    	  }
      }
      
      LOGGER.finest("Stopping EcoLawsManager cycle " + my_cycleCounter);
   }

   /**
    * Adds an eco-law to the end of the execution cycle.
    * @param an_ecoLaw the eco-law to add to the execution cycle.
    */
   public void addEcoLaw(final IEcoLaw an_ecoLaw) {
      my_ecoLaws.add(an_ecoLaw);
   }

   /**
    * Inserts an eco-law to the execution cycle at the indicated position.
    * @param an_index the position in the eco-law list to add the eco-law.
    * @param an_ecoLaw the eco-law to add to the execution cycle.
    */
   public void addEcoLaw(final int an_index, final IEcoLaw an_ecoLaw) {
      my_ecoLaws.add(an_index, an_ecoLaw);
   }

   /**
    * Replaces the eco-law set with a new set of eco-laws.
    * @param some_ecoLaws the eco-laws to run at the execution cycle.
    */
   public void replaceEcoLaws(final List<IEcoLaw> some_ecoLaws) {
      my_ecoLaws.clear();
      for (IEcoLaw law : some_ecoLaws) {
         my_ecoLaws.add(law);
      }
   }

}
