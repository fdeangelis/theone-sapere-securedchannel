/**
 * 
 */
package sapere.node.lsaspace.ecolaws;

import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.values.PropertyName;
import sapere.lsa.values.SyntheticPropertyName;
import sapere.node.NodeManager;
import sapere.node.lsaspace.EcoLawsEngine;
import sapere.node.notifier.event.PropagationEvent;

import com.google.common.base.Optional;

/**
 * @author Gabriella Castelli (UNIMORE)
 * @author Graeme Stevenson (STA)
 */
public class Propagation extends AbstractEcoLaw {

   /**
    * Creates a new instance of the propagation eco-law.
    * @param a_nodeManager the node manager of the space.
    */
   public Propagation(final NodeManager a_nodeManager) {
      super(a_nodeManager);
   }

   /**
    * Iterate through the LSAs checking for gradient and direct propagation.
    */
   public void invoke() {
      for (final Lsa lsa : getLSAs()) {
         if (isGradientPropagation(lsa)) {
            doGradientPropagation(lsa);
         } else if (isDirectPropagation(lsa)) {
            doDirectPropagation(lsa);
         }
      }
   }

   /**
    * Propagate an LSA using direct propagation. The network delivery manager checks to see if the
    * host is valid so we do not need to do that here.
    * @param an_lsa the LSA to propagate.
    */
   public void doDirectPropagation(final Lsa an_lsa) {
      final String destinationName = an_lsa.getSingleValue(PropertyName.DESTINATION.toString()).get();
      boolean successful = getNetworkDeliveryManager().sendDirect(destinationName, directPropagationCopy(an_lsa));
      if (successful) {
         PropagationEvent event = new PropagationEvent(an_lsa);
         publish(event);
      }
   }

   /**
    * Propagate an LSA using gradient propagation
    * @param an_lsa the LSA to propagate.
    */
   public void doGradientPropagation(final Lsa an_lsa) {
      getNetworkDeliveryManager().doSpread(gradientCopy(an_lsa));
      PropagationEvent event = new PropagationEvent(an_lsa);
      publish(event);
   }

   /**
    * Creates a copy of an LSA to be propagated directly, removing the id, bonds and other
    * properties.
    * @param an_lsa the LSA to be copied.
    * @return the LSA copy.
    */
   public Lsa directPropagationCopy(final Lsa an_lsa) {
      Lsa copy = an_lsa.getCopy();
      copy.removeSyntheticProperty(SyntheticPropertyName.BONDS);
      copy.removeSyntheticProperty(SyntheticPropertyName.CREATION_TIME);
      copy.removeSyntheticProperty(SyntheticPropertyName.CREATOR_ID);
      copy.removeSyntheticProperty(SyntheticPropertyName.LAST_MODIFIED);
      copy.removeSyntheticProperty(SyntheticPropertyName.LOCATION);
      copy.addProperty(new Property(PropertyName.PREVIOUS.toString(), getSpaceName()));
      copy.removeProperty(PropertyName.DESTINATION.toString());
      copy.removeProperty(PropertyName.DIFFUSION_OP.toString());
      copy.setId(null);
      copy.removeBonds();
      return copy;
   }

   /**
    * Creates a copy of an LSA to be propagated, removing the id, bonds and other properties.
    * @param an_lsa the LSA to be copied.
    * @param the_finalHop true if this is the final hop for the LSA, false otherwise.
    * @return the LSA copy.
    */
   public Lsa gradientCopy(final Lsa an_lsa) {
      Lsa copy = an_lsa.getCopy();
      copy.removeSyntheticProperty(SyntheticPropertyName.BONDS);
      copy.removeSyntheticProperty(SyntheticPropertyName.CREATION_TIME);
      copy.removeSyntheticProperty(SyntheticPropertyName.CREATOR_ID);
      copy.removeSyntheticProperty(SyntheticPropertyName.LAST_MODIFIED);
      copy.removeSyntheticProperty(SyntheticPropertyName.LOCATION);
      copy.addProperty(new Property(PropertyName.PREVIOUS.toString(), getSpaceName()));
      copy.setId(null);
      final int nextHop = getNextHop(copy);
      if (nextHop >= 1) {
         copy.addProperty(new Property(PropertyName.DIFFUSION_OP.toString(), "GRADIENT_" + nextHop));
      } else {
         // diffuse the LSA, but the diffuse LSA won't be propagated further
         copy.removeProperty(PropertyName.DIFFUSION_OP.toString());
         copy.removeProperty(PropertyName.DESTINATION.toString());
      }
      return copy;
   }

   /**
    * Whether the input LSA has direct propagation properties set.
    * @param an_lsa the LSA to check for direct propagation.
    * @return true if the LSA conforms, false otherwise.
    */
   private boolean isDirectPropagation(final Lsa an_lsa) {
      return an_lsa.getSingleValue(PropertyName.DIFFUSION_OP.toString()).isPresent()
            && "direct".equals(an_lsa.getSingleValue(PropertyName.DIFFUSION_OP.toString()).get());
   }

   /**
    * Whether the input LSA has gradient propagation properties set.
    * @param an_lsa the LSA to check for gradient propagation.
    * @return true if the LSA conforms, false otherwise.
    */
   private boolean isGradientPropagation(final Lsa an_lsa) {
      boolean result = false;
      final Optional<String> diffusionOp = an_lsa.getSingleValue(PropertyName.DIFFUSION_OP.toString());
      if (diffusionOp.isPresent()&& diffusionOp.get().indexOf("_") >0 ) {
         final String diffusionMode = diffusionOp.get().substring(0, diffusionOp.get().indexOf("_"));
         result = "GRADIENT".equals(diffusionMode);
      }
      return result;
   }

   /**
    * Gets the next hop count for a gradient LSA.
    * @param an_lsa the LSA to extract the hop count from.
    * @return the next hop count.
    */
   private int getNextHop(final Lsa an_lsa) {
      String diffusionOp = an_lsa.getSingleValue(PropertyName.DIFFUSION_OP.toString()).get();
      int currentHop = Integer.parseInt(diffusionOp.substring(diffusionOp.indexOf("_") + 1));
      return currentHop - 1;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean doesSpread() {
      return true;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean doesDirectPropagation() {
      // Technically true - but propagation rate in eco-law engine supersedes it. Possibly split
      // propagation into Spreading+Direct eco-laws?
      return false;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getName() {
      return EcoLawsEngine.ECO_LAWS_PROPAGATION;
   }
}
