/**
 * 
 */
package sapere.node.lsaspace.ecolaws;

import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.values.PropertyName;
import sapere.node.NodeManager;
import sapere.node.lsaspace.EcoLawsEngine;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.AbstractSapereEvent;

/**
 * @author Gabriella Castelli (UNIMORE)
 * @author Graeme Stevenson (STA)
 */
public class Decay extends AbstractEcoLaw {

   /**
    * Creates a new instance of the decay eco-law.
    * @param a_nodeManager the node manager of the space.
    */
   public Decay(final NodeManager a_nodeManager) {
      super(a_nodeManager);
   }

   /**
    * Invokes the decay eco-law by selecting relevant LSAs and applying the decay transformantion.
    */
   public void invoke() {
      for (Lsa lsa : getLSAs()) {
         if (lsa.getSingleValue("decay").isPresent()) {
            applyToLsa(lsa);
         }
      }
   }

   /**
    * Applies the decay transformation to an LSA.
    * @param an_lsa the LSA to decay.
    */
   public void applyToLsa(final Lsa an_lsa) {
      // decrement the TTL
      int decay = PropertyName.decrement(an_lsa.getDecayProperty());
      if (decay <= 0) {

          remove(an_lsa);
         //Triggers the DecayedEvent
         AbstractSapereEvent decayEvent = new DecayedEvent(an_lsa);
         decayEvent.setRequiringAgent(null);
         publish(decayEvent);
      } else {
         an_lsa.addProperty(new Property("decay", String.valueOf(decay)));
         update(an_lsa);
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean doesSpread() {
      return false;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean doesDirectPropagation() {
      return false;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getName() {
      return EcoLawsEngine.ECO_LAWS_DECAY;
   }
}
