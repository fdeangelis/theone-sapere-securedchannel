package sapere.node.lsaspace.ecolaws;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Vector;

import sapere.lsa.Id;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.SyntheticProperty;
import sapere.lsa.exception.UnresolvedPropertyNameException;
import sapere.lsa.values.SyntheticPropertyName;

public enum AggregationOperators {

   NEWEST {
      public String toString() {
         return "NEWEST";
      }
   },

   OLDEST {
      public String toString() {
         return "OLDEST";
      }
   },

   MAX {
      public String toString() {
         return "MAX";
      }
   },

   MIN {
      public String toString() {
         return "MIN";
      }
   },

   AVG {
      public String toString() {
         return "AVG";
      }
   };

   private static BigDecimal getValue(Lsa l, String fieldName) {

      if (SyntheticPropertyName.isSyntheticProperty(fieldName)) {
         return new BigDecimal(l.getSyntheticProperty(SyntheticPropertyName.getSyntheticPropertiesValues(fieldName))
               .getValue().elementAt(0));

      } else
         return new BigDecimal(l.getProperty(l.getFieldValue()).getValue().elementAt(0));
   }

   public static Id max(Vector<Lsa> allLsa) {

      Id ret = null;

      if (allLsa.isEmpty())
         return null;

      BigDecimal max = getValue(allLsa.elementAt(0), allLsa.elementAt(0).getFieldValue());
      ret = allLsa.elementAt(0).getId();

      for (int i = 0; i < allLsa.size(); i++) {
         BigDecimal m = getValue(allLsa.elementAt(i), allLsa.elementAt(0).getFieldValue());
         if (m.max(max).equals(m)) {
            max = m;
            ret = allLsa.elementAt(i).getId();
         }
      }

      return ret;

   }

   /*
    * public static Lsa max (Vector<Lsa> allLsa, String fieldValue){ Lsa ret = null;
    * if(allLsa.isEmpty()) return null; int max = new
    * Integer(allLsa.elementAt(0).getProperty(fieldValue).getValue().elementAt(0)).intValue(); ret =
    * allLsa.elementAt(0).getCopy(); for(int i=0; i<allLsa.size(); i++){ if (new
    * Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue() >
    * max){ max = new
    * Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue(); ret =
    * allLsa.elementAt(i).getCopy(); } } return ret; }
    */

   public static String maxValue(Vector<Lsa> allLsa, String fieldValue) {

      Lsa ret = null;

      if (allLsa.isEmpty())
         return null;

      int max = new Integer(allLsa.elementAt(0).getProperty(fieldValue).getValue().elementAt(0)).intValue();
      ret = allLsa.elementAt(0).getCopy();

      for (int i = 0; i < allLsa.size(); i++) {
         if (new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue() > max) {
            max = new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue();
            ret = allLsa.elementAt(i).getCopy();
         }
      }

      return new Integer(max).toString();

   }

   public static Id min(Vector<Lsa> allLsa) {

      Id ret = null;

      if (allLsa.isEmpty())
         return null;

      BigDecimal min = getValue(allLsa.elementAt(0), allLsa.elementAt(0).getFieldValue());
      ret = allLsa.elementAt(0).getId();

      for (int i = 0; i < allLsa.size(); i++) {
         BigDecimal m = getValue(allLsa.elementAt(i), allLsa.elementAt(0).getFieldValue());
         if (m.min(min).equals(m)) {
            min = m;
            ret = allLsa.elementAt(i).getId();
         }
      }

      return ret;

   }

   /*
    * public static Lsa min (Vector<Lsa> allLsa, String fieldValue){ Lsa ret = null;
    * if(allLsa.isEmpty()) return null; int min = new
    * Integer(allLsa.elementAt(0).getProperty(fieldValue).getValue().elementAt(0)).intValue(); ret =
    * allLsa.elementAt(0).getCopy(); for(int i=0; i<allLsa.size(); i++){ if (new
    * Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue() <
    * min){ min = new
    * Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue(); ret =
    * allLsa.elementAt(i).getCopy(); } } return ret; }
    */

   public static String minValue(Vector<Lsa> allLsa, String fieldValue) {

      Lsa ret = null;
      if (allLsa.isEmpty())
         return null;
      int min = new Integer(allLsa.elementAt(0).getProperty(fieldValue).getValue().elementAt(0)).intValue();
      ret = allLsa.elementAt(0).getCopy();

      for (int i = 0; i < allLsa.size(); i++) {
         if (new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue() < min) {
            min = new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue();
            ret = allLsa.elementAt(i).getCopy();
         }
      }

      return new Integer(min).toString();

   }

   public static Lsa avg(Vector<Lsa> allLsa) {

      Lsa ret = null;

      if (allLsa.isEmpty())
         return null;

      BigDecimal val = new BigDecimal("0");

      for (int i = 0; i < allLsa.size(); i++) {
         val = val.add(new BigDecimal(allLsa.elementAt(i).getProperty(allLsa.elementAt(0).getFieldValue()).getValue()
               .elementAt(0)));
      }

      val = val.divide(new BigDecimal(allLsa.size()));

      ret = allLsa.elementAt(0).getCopy();
      try {
         ret.setProperty(new Property(allLsa.elementAt(0).getFieldValue(), "" + val));
      } catch (UnresolvedPropertyNameException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

      ret.addProperty(new SyntheticProperty(SyntheticPropertyName.LAST_MODIFIED, new String("" + new Date().getTime())));

      return ret;

   }

   public static String avgValue(Vector<Lsa> allLsa, String fieldValue) {

      Lsa ret = null;

      if (allLsa.isEmpty())
         return null;

      int val = 0;

      for (int i = 0; i < allLsa.size(); i++) {
         val += new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue();
      }

      val = val / allLsa.size();

      ret = allLsa.elementAt(0).getCopy();
      try {
         ret.setProperty(new Property(fieldValue, "" + val));
      } catch (UnresolvedPropertyNameException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

      ret.addProperty(new SyntheticProperty(SyntheticPropertyName.LAST_MODIFIED, new String("" + new Date().getTime())));

      return new Integer(val).toString();

   }

}
