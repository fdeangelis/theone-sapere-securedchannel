/**
 * 
 */
package sapere.node.lsaspace.ecolaws.spreading;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import one.core.DTNHost;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.values.PropertyName;
import sapere.lsa.values.SyntheticPropertyName;
import sapere.node.NodeManager;
import sapere.node.lsaspace.EcoLawsEngine;
import sapere.node.lsaspace.ecolaws.AbstractEcoLaw;
import sapere.node.notifier.event.PropagationEvent;

import com.google.common.base.Optional;

import extension.SapereProperties;

/**
 * The framework for a spreading eco-law. Contains control logic, with decision
 * making carried out by subclasses.
 * 
 * @author Graeme Stevenson (STA)
 */
public abstract class AbstractSpreadingEcoLaw extends AbstractEcoLaw {

	/**
	 * A list containing the IDs of LSAs that have either 1) already been spread
	 * from this node, or 2) a decision has been taken not to spread them from
	 * this node. This is only suitable for experiment purposes, where the total
	 * number of LSAs is not expected to be large. Otherwise, some form of
	 * cleanup is required.
	 */
	private List<UUID> ignoreLsaIDs = new LinkedList<UUID>();

	/**
	 * The node hosting this eco-law instance.
	 */
	private final DTNHost my_host;

	/**
	 * Creates a new instance of the location based spreading eco-law.
	 * 
	 * @param a_nodeManager
	 *            the node manager of the space.
	 * @param a_dtnHost
	 *            the hosting node.
	 * @param dtnHost
	 */
	public AbstractSpreadingEcoLaw(final NodeManager a_nodeManager,
			final DTNHost a_dtnHost) {
		super(a_nodeManager);
		my_host = a_dtnHost;
	}

	/**
	 * Iterate through the LSAs checking for any to be spread. Add to the ignore
	 * list after processing.
	 */
	public final void invoke() {
		for (final Lsa lsa : getLSAs()) {
			int h;
			if (lsa.getProperty("Agent-hh") != null
					&& 
					lsa.getProperty("Agent-hh").
					getValue().get(0).equals("AgentReader2"))
			{
				h = 3;
			}
			if (shouldLsaBeSpread(lsa)) {
				doSpread(lsa);
			}
			// If we wish propagate once, we add the LSA to the ignore list.
			if (SapereProperties.PROPAGATE_ONCE_ONLY) {
				addLSAToIgnoreList(lsa);
			}
		}
	}

	/**
	 * Subclasses should implement to control whether or not an LSA is spread.
	 * 
	 * @param lsa
	 *            the LSA to check.
	 * @return true if the LSA should be spread, false otherwise.
	 */
	abstract boolean shouldLsaBeSpread(final Lsa lsa);

	/**
	 * Adds an LSA to the ignore list, identified by its UUID.
	 * 
	 * @param an_lsa
	 *            the LSA to ignore.
	 */
	private void addLSAToIgnoreList(final Lsa an_lsa) {
		// Add this LSA UUID to the ignore list
		if (an_lsa.hasProperty("uuid")) {
			ignoreLsaIDs.add(UUID.fromString(an_lsa.getSingleValue("uuid")
					.get()));
		}
	}

	/**
	 * Propagate an LSA using gradient propagation
	 * 
	 * @param an_lsa
	 *            the LSA to propagate.
	 */
	private void doSpread(final Lsa an_lsa) {
		// Make a copy of the LSA to be spread
		final Lsa lsaCopy = getLsaCopy(an_lsa);
		// Tag the copy with the coordinates of this node.
		beforeSpreading(lsaCopy);
		// Spread the LSA
		getNetworkDeliveryManager().doSpread(lsaCopy);
		// Do notifications
		PropagationEvent event = new PropagationEvent(an_lsa);
		publish(event);
	}

	/**
	 * Called just before an LSA is spread to support any additional processing.
	 * 
	 * @param lsa
	 *            the LSA that will be spread, which is a copy of the original.
	 */
	abstract void beforeSpreading(Lsa lsaCopy);

	/**
	 * Creates a copy of an LSA to be propagated directly, removing the id,
	 * bonds and other properties.
	 * 
	 * @param an_lsa
	 *            the LSA to be copied.
	 * @return the LSA copy.
	 */
	private final Lsa getLsaCopy(final Lsa an_lsa) {
		Lsa copy = an_lsa.getCopy();
		copy.removeSyntheticProperty(SyntheticPropertyName.BONDS);
		copy.removeSyntheticProperty(SyntheticPropertyName.CREATION_TIME);
		copy.removeSyntheticProperty(SyntheticPropertyName.CREATOR_ID);
		copy.removeSyntheticProperty(SyntheticPropertyName.LAST_MODIFIED);
		copy.removeSyntheticProperty(SyntheticPropertyName.LOCATION);
		copy.addProperty(new Property(PropertyName.PREVIOUS.toString(),
				getSpaceName()));
		copy.removeProperty(PropertyName.DESTINATION.toString());
		copy.removeProperty(PropertyName.DIFFUSION_OP.toString());
		copy.setId(null);
		copy.removeBonds();
		// Add hop count infermation
		incrementHopCount(copy);

		return copy;
	}

	private void incrementHopCount(final Lsa an_lsa) {
		Optional<String> hopsProperty = an_lsa.getSingleValue("hopsTravelled");
		if (hopsProperty.isPresent()) {
			int currentHop = Integer.parseInt(hopsProperty.get());
			an_lsa.addProperty(new Property("hopsTravelled", String
					.valueOf(currentHop + 1)));
		} else {
			an_lsa.addProperty(new Property("hopsTravelled", String.valueOf(1)));
		}
	}

	/**
	 * Decides whether an LSA is a candidate for spreading. An LSA is a
	 * candidate if its spreading property is set to true and it has not already
	 * been spread by this node.
	 * 
	 * @param an_lsa
	 *            the LSA we wish to check for spreadability.
	 * @return true if the LSA conforms, false otherwise.
	 */
	public boolean isSpreadable(final Lsa an_lsa) {
		final Optional<String> spreadOp = an_lsa.getSingleValue("spread");
		return spreadOp.isPresent() && "true".equals(spreadOp.get());

	}

	/**
	 * Decides whether an LSA is a candidate for spreading. An LSA is a
	 * candidate if its spreading property is set to true and it has not already
	 * been spread by this node.
	 * 
	 * @param an_lsa
	 *            the LSA we wish to check for spreadability.
	 * @return true if the LSA conforms, false otherwise.
	 */
	public boolean hasNotAlreadySpread(final Lsa an_lsa) {

		final Optional<String> lsaID = an_lsa.getSingleValue("uuid");
		return lsaID.isPresent()
				&& !ignoreLsaIDs.contains(UUID.fromString(lsaID.get()));
	}

	/**
	 * Decides whether an LSA is a candidate for spreading. An LSA is a
	 * candidate if its spreading property is set to true and it has not already
	 * been spread by this node.
	 * 
	 * @param an_lsa
	 *            the LSA we wish to check for spreadability.
	 * @return true if the LSA conforms, false otherwise.
	 */
	public boolean decayIsMaximum(final Lsa an_lsa) {

		final String lsaDecay = an_lsa.getProperty("decay").getValue()
				.elementAt(0);
		if (Integer.valueOf(lsaDecay) > Integer.valueOf(SapereProperties.DECAY_VALUE)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean doesSpread() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean doesDirectPropagation() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return EcoLawsEngine.ECO_LAWS_PROPAGATION;
	}

	/**
	 * Returns the hosting node.
	 * 
	 * @return the node hosting the eco-law.
	 */
	public DTNHost getHost() {
		return my_host;
	}
}
