/**
 * 
 */
package sapere.node.notifier;

import sapere.node.notifier.filter.IFilter;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class Subscription {
	
	// Stores information about a single subscription
		
	public Class eventType;
	public IFilter filter;
	public AbstractSubscriber subscriber;
	public String subscriberName;
	
	 public Subscription(Class anEventType, IFilter aFilter, AbstractSubscriber aSubscriber, String subscriberName) {
		 this.eventType = anEventType;
		 this.filter = aFilter;
		 this.subscriber = aSubscriber;
		 this.subscriberName = subscriberName;
	  }
		  
	public AbstractSubscriber getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(AbstractSubscriber subscriber) {
		this.subscriber = subscriber;
	}
	
	@Override
	public boolean equals (Object o){
		 boolean ret = false;
		  
		 if(o instanceof Subscription)
			 ret = ( eventType.equals(((Subscription) o).eventType) &&
					 filter.equals(((Subscription) o).filter) &&
					 subscriberName.equals(((Subscription) o).subscriberName)
					 );
		  
		  return ret;
	  }

	  @Override
	  public String toString(){
		  String ret = new String();
		  
		  ret = "<eventType="+eventType+", filter="+filter+", subscriber="+subscriber+", subscriberName="+subscriberName+">";
		  
		  return ret;
	  }	  

}
