/**
 * 
 */
package sapere.node.notifier.event;

import sapere.lsa.Lsa;


/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class AggregationRemovedEvent extends AbstractSapereEvent{
	
	private Lsa lsa = null;

	public AggregationRemovedEvent(Lsa lsa){
		this.lsa = lsa;
	}
	
	public Lsa getLsa(){
		return lsa;
	}

}
