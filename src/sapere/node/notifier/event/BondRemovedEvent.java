/**
 * 
 */
package sapere.node.notifier.event;

import sapere.lsa.Lsa;
import sapere.lsa.interfaces.ILsa;


/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class BondRemovedEvent extends AbstractSapereEvent{
	
	private Lsa lsa = null;
	private String bondId = null; // the the Id of the LSa to which I have just been bonded

	public BondRemovedEvent(Lsa lsa, String bondId){
		this.lsa = lsa;
		this.bondId = bondId;
	}
	
	public ILsa getLsa(){
		return lsa;
	}
	
	public String getBondId(){
		return bondId;
	}


}