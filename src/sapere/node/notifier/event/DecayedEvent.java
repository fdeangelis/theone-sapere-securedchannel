/**
 * 
 */
package sapere.node.notifier.event;

import sapere.lsa.Lsa;


/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class DecayedEvent extends AbstractSapereEvent{
	
	private Lsa lsa = null;

	public DecayedEvent(Lsa lsa){
		this.lsa = lsa;
	}
	
	public Lsa getLsa(){
		return lsa;
	}

}
