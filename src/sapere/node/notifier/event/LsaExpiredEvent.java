/**
 * 
 */
package sapere.node.notifier.event;

import sapere.lsa.Lsa;


/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class LsaExpiredEvent extends AbstractSapereEvent{
	
	private Lsa lsa = null;

	public LsaExpiredEvent(Lsa lsa){
		this.lsa = lsa;
	}
	
	public Lsa getLsa(){
		return lsa;
	}

}
