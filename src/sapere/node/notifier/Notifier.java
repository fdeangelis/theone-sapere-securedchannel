package sapere.node.notifier;

import java.util.Enumeration;
import java.util.Vector;
import java.util.logging.Logger;

import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.IEvent;
import sapere.node.notifier.event.AbstractSapereEvent;
import sapere.node.notifier.exception.InvalidEventTypeException;
import sapere.node.notifier.filter.BondedLsaUpdateFilter;


/**
 * @author Gabriella Castelli (UNIMORE)
 */
public class Notifier implements INotifier {

   private static final Logger LOGGER = Logger.getLogger("sapere.node.notifier");

   private Class<IEvent> eventClass = null;

   protected Vector<Subscription> subscriptions = null;

   // Prevents direct instantiation of the event service
   public Notifier() {
      eventClass = IEvent.class;
      subscriptions = new Vector<Subscription>();
   }

   public void publish(AbstractSapereEvent event) {

      for (Enumeration<Subscription> elems = subscriptions.elements(); elems.hasMoreElements();) {
    	  
         Subscription subscription = elems.nextElement();

         if (subscription.eventType.equals(BondedLsaUpdateEvent.class) && (event instanceof BondedLsaUpdateEvent)) {

            if (subscription.filter.apply(event, subscription.subscriberName)) {
               subscription.subscriber.onNotification(event);
            }

         } else {
        	 
        	 if (subscription.eventType.isAssignableFrom(event.getClass())
                  && (subscription.filter == null || subscription.filter.apply(event))) {
        		 try {
        			 subscription.subscriber.onNotification(event);
        			 } catch (NullPointerException e) {
        				 LOGGER.warning("Caught notification to agent (Notifier.java): " + event.getClass());
        				 if (event instanceof BondAddedEvent) {
        					 LOGGER.info("requiring agent: " + event.getRequiringAgent());
        				 }
        			 }

        	 }

         }// end for
      }
   }

   public void subscribe(Subscription s) throws InvalidEventTypeException {

      if (!eventClass.isAssignableFrom(s.eventType))
         throw new InvalidEventTypeException();
      
      boolean contains = false;
      
      for(Subscription ss : subscriptions)
    	  if( ss.equals(s))
    		  contains = true;
    	 
      if(!contains)
    		  subscriptions.addElement(s);
  }

   public void unsubscribe(Subscription s) throws InvalidEventTypeException {

      boolean b = false;

      if (!eventClass.isAssignableFrom(s.eventType))
         throw new InvalidEventTypeException();
      b = subscriptions.remove(s);
   }

   public synchronized void unsubscribe(BondedLsaUpdateFilter filter) {

      Vector<Integer> v = new Vector<Integer>();

      for (int i = 0; i < subscriptions.size(); i++) {
         Subscription s = subscriptions.elementAt(i);

         if (s.filter.getClass().equals(filter.getClass())) {
            BondedLsaUpdateFilter f = (BondedLsaUpdateFilter) s.filter;

            if (f.getRequestingId().equals(filter.getRequestingId())
                  && f.getTargetLsaId().toString().equals(filter.getTargetLsaId().toString()))
               v.add(i);
         }

      }

      for (int j = 0; j < v.size(); j++) {
         subscriptions.removeElementAt(v.elementAt(j));
      }

   }

   public String toString() {
      String ret = null;
      for (Enumeration<Subscription> elems = subscriptions.elements(); elems.hasMoreElements();) {
         Subscription subscription = elems.nextElement();

         if (ret == null)
            ret = new String();
         ret += "\t" + subscription.subscriberName + " " + subscription.eventType + "\n";
      }

      return ret;
   }

   public String test() {
      return "" + Math.random();
   }

}
