package sapere.node.notifier;

import sapere.node.notifier.event.IEvent;


/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public abstract class AbstractSubscriber {
	
  public abstract void onNotification (IEvent event);
  
}
