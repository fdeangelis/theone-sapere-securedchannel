package sapere.node.notifier.filter;

import sapere.lsa.Id;
import sapere.lsa.Lsa;
import sapere.node.notifier.event.IEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.UpdateEvent;

public class PropagationFilter implements IFilter{
	
	private Id targetLsaId = null;
	
	public PropagationFilter (Id targetLsaId){
		this.targetLsaId = targetLsaId;
	}
    //override
	public boolean apply(IEvent event) {
		
		boolean ret = false;
		
		PropagationEvent pEvent = (PropagationEvent) event;
		
		if (((Lsa)  pEvent.getLsa()).getId().toString().equals(targetLsaId.toString()))
			ret = true;
		return ret;
	}

    //override
	public boolean apply(IEvent event, String agentName) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean equals (Object o){
		boolean ret = false; 
		if (o instanceof PropagationFilter){
		ret = ( targetLsaId.toString().equals( ((PropagationFilter) o).targetLsaId.toString()));
		}
		return ret;
	}

}
