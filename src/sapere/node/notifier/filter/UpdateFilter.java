/**
 * 
 */
package sapere.node.notifier.filter;

import sapere.lsa.Id;
import sapere.node.notifier.event.IEvent;
import sapere.node.notifier.event.UpdateEvent;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */

public class UpdateFilter implements IFilter{
	
	private Id targetLsaId = null;
	private String requestingId = null;
	
	public UpdateFilter(Id lsaId, String requestingId){
		this.targetLsaId = lsaId;
		this.requestingId = requestingId;
	}

	
	public boolean apply(IEvent event) {
		
		boolean ret = false;
		
	    UpdateEvent updateEvent = (UpdateEvent) event;
	    
	    if (updateEvent.getLsa().getId().toString().equals(targetLsaId.toString()) && updateEvent.getRequiringAgent().equals(requestingId))
	    	ret = true;
		
		return ret;
	}


	public boolean apply(IEvent event, String lsaSubscriberId) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean equals (Object o){
		boolean ret = false; 
		if (o instanceof UpdateFilter){
		ret = ( targetLsaId.toString().equals( ((UpdateFilter) o).targetLsaId.toString()) &&
				requestingId.equals(((UpdateFilter) o).requestingId));
		}
		return ret;
	}

}
