/**
 * 
 */
package sapere.node.notifier.filter;

import sapere.lsa.Id;
import sapere.node.notifier.event.AggregationRemovedEvent;
import sapere.node.notifier.event.IEvent;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */

public class AggregationRemovedFilter implements IFilter{
	
	private Id targetLsaId = null;
	private String requestingId = null;
	
	public AggregationRemovedFilter(Id lsaId, String requestingId){
		this.targetLsaId = lsaId;
		this.requestingId = requestingId;
	}

	
	public boolean apply(IEvent event) {
		boolean ret = false;
	
		AggregationRemovedEvent aggregationRemovedEvent = (AggregationRemovedEvent) event;
	    
	    if (aggregationRemovedEvent.getLsa().getId().toString().equals(targetLsaId.toString()) )
	    	ret = true;
		
		return ret;
	}


	public boolean apply(IEvent event, String lsaSubscriberId) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean equals (Object o){
		boolean ret = false; 
		
		if (o instanceof AggregationRemovedFilter)
		ret = ( targetLsaId.toString().equals( ((AggregationRemovedFilter) o).targetLsaId.toString()) &&
				requestingId.equals(((AggregationRemovedFilter) o).requestingId));
		
		return ret;
	}

}
