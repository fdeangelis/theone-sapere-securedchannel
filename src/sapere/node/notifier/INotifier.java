package sapere.node.notifier;

import sapere.node.notifier.event.AbstractSapereEvent;
import sapere.node.notifier.exception.InvalidEventTypeException;
import sapere.node.notifier.filter.BondedLsaUpdateFilter;

public interface INotifier {
	
	  public void publish(AbstractSapereEvent event);
	  
	  public void subscribe(Subscription s) throws InvalidEventTypeException;
	  
	  public void unsubscribe(Subscription s) throws InvalidEventTypeException;
	  
	  public void unsubscribe(BondedLsaUpdateFilter filter);
	  
	  public String toString();
	  
	  public String test();

}