package sapere.agent;

import sapere.lsa.Id;
import sapere.lsa.Lsa;
import sapere.node.lsaspace.Agent;
import sapere.node.lsaspace.Operation;
import sapere.node.lsaspace.OperationManager;
import sapere.node.notifier.event.IEvent;

/** 
 * The implementation provided for Data LSA.
 * Once a Data LSA is injected in the Space, it is
 * no longer on control of the entity that injected it, i.e.,
 * the events that happens to the LSA are not reported
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public  class BasicSapereAgent extends Agent{
	
	/** The reference to the local node's Operation Manager */
	private OperationManager opMng; 
	
	
	
	/** 
	 * Instantiates a Basic Sapere Agent
	 * @param agentName The name of the Agent
	 */
	public BasicSapereAgent(String agentName, OperationManager an_opMng){
		super(agentName);
		this.opMng = an_opMng;
	}	

	/**
	 * Injects the given LSA in the local Lsa space
	 * @param lsa The LSA to be injected 
	 * @return The id of the injected LSA
	 */
	public Id injectOperation (Lsa lsa){
		Id lsaId = null;
	
		Operation op = new Operation().injectOperation((Lsa)lsa, getAgentName(),  this);
		lsaId = opMng.queueOperation(op);
		lsa.setId(lsaId);
		return lsaId;
	}


	/** 
	 * Updates an LSA
	 * @param lsa The new LSA 
	 * @param lsaId The id of the LSA to be updated
	 */
	public void updateOperation (Lsa lsa, Id lsaId){
		Operation op = new Operation().updateOperation(lsa, lsaId, getAgentName(), this);
		opMng.queueOperation(op);
	}	
	
	/**
	 * Removes the specified LSA from the local LSA space
	 * 
	 * @param lsa
	 */
	public void removeLsa(Lsa lsa){
		
		Operation op = new Operation().removeOperation(lsa.getId(), getAgentName());
		
		opMng.queueOperation(op);
	}

	@Override
	public void onNotification(IEvent event) {
		// TODO Auto-generated method stub
		
	}
	

	
}
