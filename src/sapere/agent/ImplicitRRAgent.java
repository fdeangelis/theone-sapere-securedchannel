package sapere.agent;

import java.util.Vector;

import sapere.agent.implicitirr.IRRAsynchronousService;
import sapere.agent.implicitirr.IRRService;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.lsa.exception.UnresolvedPropertyNameException;
import sapere.node.lsaspace.OperationManager;
import sapere.node.notifier.Notifier;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.PropagationEvent;

/**
 * Abstract Class for Services that implements the Implicit Request-Response interaction Pattern
 * @author Gabriella Castelli (UNIMORE)
 */
public abstract class ImplicitRRAgent extends LsaAgent {

   private String potentialPropertyName = null;

   private String formalPropertyName = null;

   private IRRService service = null;

   private IRRAsynchronousService complexService = null;

   /**
    * @param name The name of this Agent
    */
   public ImplicitRRAgent(String name, OperationManager an_opMng, Notifier notifier) {

      super(name, an_opMng, notifier);
   }

   /**
    * Use this method to set the initial content of the LSA managed by this Agent.
    */
   public abstract void setInitialLSA();

   private void addLsaProperty(String potentialPropertyName, String formalPropertyName, IRRAsynchronousService service) {
      this.potentialPropertyName = potentialPropertyName;
      this.formalPropertyName = formalPropertyName;
      this.complexService = service;

      Property potentialProperty = new Property(potentialPropertyName, "!");
      Property formalProperty = new Property(formalPropertyName, "?");

      lsa.addProperty(potentialProperty);
      lsa.addProperty(formalProperty);

   }

   private void addLsaProperty(String potentialPropertyName, String formalPropertyName, IRRService service) {
      this.potentialPropertyName = potentialPropertyName;
      this.formalPropertyName = formalPropertyName;
      this.service = service;

      Property potentialProperty = new Property(potentialPropertyName, "!");
      Property formalProperty = new Property(formalPropertyName, "?");

      lsa.addProperty(potentialProperty);
      lsa.addProperty(formalProperty);

   }

   /**
    * Adds the required Properties to this Agent
    * @param potentialPropertyName
    * @param formalPropertyName
    * @param service
    */
   public void addProperty(String potentialPropertyName, String formalPropertyName, IRRService service) {

      addLsaProperty(potentialPropertyName, formalPropertyName, service);

      submitOperation();
   }

   /**
    * Adds the required PRoperties to this Agent
    * @param potentialPropertyName
    * @param formalPropertyName
    * @param service
    */
   public void addProperty(String potentialPropertyName, String formalPropertyName, IRRAsynchronousService service) {

      addLsaProperty(potentialPropertyName, formalPropertyName, service);

      submitOperation();
   }

   private void setFormalPropertyValue(String value) {
      setProperty(new Property(formalPropertyName, value), false);
   }

   private void setFormalPropertyValue(Vector<String> value) {
      setProperty(new Property(formalPropertyName, value), false);
   }

   private void resetFormalPropertyValue(boolean updateLsa) {
      setProperty(new Property(formalPropertyName, "!"), updateLsa);
   }

   public void setPotentialPropertyValue(String value) {
      setProperty(new Property(potentialPropertyName, value), true);
   }

   public void setPotentialPropertyValue(Vector<String> value) {
      setProperty(new Property(potentialPropertyName, value), true);
   }

   private void resetPotentialPropertyValue(boolean updateLsa) {
      setProperty(new Property(potentialPropertyName, "?"), updateLsa);
   }

   /**
    * Resets the Request-Reply Properties
    */
   public void resetService() {
      resetFormalPropertyValue(false);
      resetPotentialPropertyValue(true);
   }

   private void setProperty(Property p, boolean updateLsa) {

      try {
         lsa.setProperty(p);
         if (updateLsa)
            submitOperation();
      } catch (UnresolvedPropertyNameException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

   }

   /*
    * public void addProperty(String potentialPropertyName, String formalPropertyName, IRRService
    * service, Property... properties){ addLsaProperty(potentialPropertyName, formalPropertyName,
    * service); Property p[] = properties; for (int i=0; i<p.length;i++){ lsa.addProperty(p[i]); }
    * submitOperation (); }
    */

   public void onBondAddedNotification(BondAddedEvent event) {

      // String formalPropertyValue =
      // event.getBondedLsa().getProperty(formalPropertyName).getValue().elementAt(0);
      // setFormalPropertyValue(formalPropertyValue);

      Vector<String> formalPropertyValue = event.getBondedLsa().getProperty(formalPropertyName).getValue();
      setFormalPropertyValue(formalPropertyValue);

      if (service != null)
         setPotentialPropertyValue(service.provideResponse((Lsa) event.getBondedLsa()));
      else
         complexService.onBondedLsaNotification((Lsa) event.getBondedLsa());
   }

   public void onBondRemovedNotification(BondRemovedEvent event) {

      this.resetService();

   }

   public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
   }

   public void onPropagationEvent(PropagationEvent event) {
      // TODO Auto-generated method stub

   }

}
